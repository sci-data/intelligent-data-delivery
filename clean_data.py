import re, json

# dictionary to store anonymous USER* name
name_dict = {}
counter = 0
replace_name = ""

DIR = "/PycharmProjects/ML_Classification/"
RAW_DATA_NAME = "ooi_data2"
# Read file
# input raw data // your need to replace the directory
raw_data = open(DIR + RAW_DATA_NAME, 'r')
# create a new file to store the processed data
anonymous_data = open('/PycharmProjects/ML_Classification/anonymous_ooi_data', 'w+')

# read first line of data
line = raw_data.readline()

while line:
    # to avoid empty line
    if line.strip():
        #user_name_temp = re.search(r"(\w+\%40\w+\.\w+)|(\w+@\w+\.\w+)", line)
        user_name_temp = re.search(r'(user=.*?,)|(user=.*?&)', line)
        # To detect whether this line has user information
        if not user_name_temp:
            # write this line to anonymous file
            anonymous_data.write(line)
            # read next line
            line = raw_data.readline()
            continue
        else:
            # extract the user name
            user_name = user_name_temp.group(0)
            #Replace '%40' with @
            if re.search(r'\%40', user_name):
                user_name = re.sub(r'%40', '@', user_name)


        # To detect whether this user has been recorded
        if user_name not in name_dict:
            # create a new name e.g., USER0, USER1, USER2 ....
            replace_name = 'USER' + str(counter) + ','
            counter += 1
            name_dict[user_name] = replace_name

            # Replace the user name and email address with USER*
            new_line = re.sub(r"(user=.*?,)|(user=.*?&)", replace_name, line)

            #If there is 'email=' attribute
            if re.search(r'(email=.*?,)|(email=.*?&)|(email=.*$)', new_line):
                new_line = re.sub(r"(email=.*?,)|(email=.*?&)|(email=.*$)", replace_name, new_line)

            # Write to the new file
            anonymous_data.write(new_line)
        # If the user has been recorded, get from hash table
        else:
            replace_name = name_dict[user_name]
            new_line = re.sub(r"(user=.*?,)|(user=.*?&)", replace_name, line)
            # If there is 'email=' attribute
            if re.search(r'(email=.*?,)|(email=.*?&)|(email=.*$)', new_line):
                new_line = re.sub(r"(email=.*?,)|(email=.*?&)|(email=.*$)", replace_name, new_line)

            anonymous_data.write(new_line)

        # Read next line
        line = raw_data.readline()
    # If the line was empty, read next line
    else:
        line = raw_data.readline()

# Close file
raw_data.close()
anonymous_data.close()

# Dump the user name
with open(DIR + "user_info.txt", 'w+') as user_name_file:
    user_name_file.write(json.dumps(name_dict))
