from __future__ import division # compate division in python 2.6+

import time, csv, glob
import pandas as pd
import collections

from statsmodels.tsa.arima_model import ARIMA


DEBUG_FLAG = False

## OOI Config
DIR = "/data_csv/tmp_simulation/ooi_tmp/"
OUTPUT_DIR = "/data_csv/tmp_simulation/ooi_tmp_res/"
STAT_DIR = "/data_csv/tmp_simulation/"
INPUT_FILE = ""
OUTPUT_FILE = ""
STATISTIC_DOC = 'OOI_prediction_statisitics.csv'
FAIL_LIST = "OOI_prediction_failed_list.csv"

file_list = [] # get the input files
prediction_accuracy_dict = collections.defaultdict() # {file_name: accuracy}
prediction_failure_list = [] # [failed_file_name]


init_data_size = 30  # The size to have the first training data
history_size = 2 * init_data_size  # history window to be kept for training every time

P, D, Q = 1, 1, 0 # ARIMA parameters

# Get the list of files
file_list = glob.glob1(DIR, "*.csv")

file_cnt = 0 # count the file

# Read input file name from the file list
for file in file_list:
    INPUT_FILE = file.rstrip()
    OUTPUT_FILE = INPUT_FILE

    file_cnt += 1
    print("Read #%s file: %s" %(file_cnt, INPUT_FILE))

    t_start = time.time()

    # if DEBUG_FLAG:
    #     t_start_1 = time.time()

    log_name = DIR + INPUT_FILE
    # UNAVCO pattern
    log_input_df = pd.read_csv(DIR+INPUT_FILE, names=['TS', 'USER_ID', 'OBJ_ID', 'SIZE', 'TRANS_TIME', 'REQ_TYPE', 'TS_DIFF', 'FLAG']) # FLAG indicate whether the TS_DIFF is valid
    # OOI pattern
    log_input_df = pd.read_csv(DIR + INPUT_FILE,
                               names=['TS', 'USER_ID', 'OBJ_ID', 'T_START', 'T_END', 'REQ_TYPE', 'TS_DIFF',
                                      'FLAG'])  # FLAG indicate whether the TS_DIFF is valid

    # Convert the "REQ_TYPE" into string
    log_input_df["REQ_TYPE"] = log_input_df["REQ_TYPE"].astype(str)
    log_input_df["FLAG"] = log_input_df["FLAG"].astype(str)
    log_input_df["TS_DIFF"] = log_input_df["TS_DIFF"].fillna(0)

    # ge to time interval, in float
    tmp_length = len(log_input_df.index)

    # Check file length, if less than seq_length, treat it as the failed prediction.
    # Because some files are small
    if tmp_length < init_data_size:
        print("File is too short: ", INPUT_FILE)
        prediction_failure_list.append(INPUT_FILE)
        continue

    # Convert timestamp into time interval
    for i in range(tmp_length-1):
        tmp_diff = log_input_df['TS'][i+1] - log_input_df['TS'][i]

        # Filter out outlier
        if tmp_diff <= 60:
            # Too short, treat it as prefetched, but filter it out from the ARIMA input sequence
            log_input_df.at[i, 'TS_DIFF'] = tmp_diff
            log_input_df.at[i, 'REQ_TYPE'] = 'p'
            log_input_df.at[i, 'FLAG'] = 'F'

        elif tmp_diff >= 86400: # One day in second
            # Too long, treat it as unprefetched, and filter it out from the ARIMA input sequence
            log_input_df.at[i, 'TS_DIFF'] = tmp_diff
            log_input_df.at[i, 'REQ_TYPE'] = 'n'
            log_input_df.at[i, 'FLAG'] = 'F'

        else: # normal
            log_input_df.at[i, 'TS_DIFF'] = tmp_diff
            log_input_df.at[i, 'FLAG'] = 'T'

    # Last element
    log_input_df.at[tmp_length-1, 'TS_DIFF'] = 1 # for division safe
    log_input_df.at[tmp_length-1, 'REQ_TYPE'] = 'n'
    log_input_df.at[tmp_length-1, 'FLAG'] = 'F'


    # if DEBUG_FLAG:
    #     t_end_1 = time.time()
    #     print("Time of preprocess files: ", t_end_1 - t_start_1)
    #     print("Start processing the file")



    # ARIMA based prediction
    # t_begin = time.time()
    history = []
    good_cnt = 0 # prediction succeed counter
    tot_cnt = 0 # total data point counter
    off_set = 0.7  # prefetch offset, move data in at off_set% of the predicted time.
    # Move the data at 70% of predicted time
    try:
        for t in range(len(log_input_df.index)-1): # skip the last row
            if log_input_df.at[t, 'FLAG'] == 'F': # skip
                # Don't count this data point in calculating the prediction accuracy rate
                continue

            if len(history) <= init_data_size:
                history.append(log_input_df.at[t, 'TS_DIFF'])
                log_input_df.at[t, 'REQ_TYPE'] = 'n'
                continue

            # Count the data point
            tot_cnt += 1

            # if DEBUG_FLAG:
            #     print(history)

            model = ARIMA(history, order=(P, D, Q))
            # if DEBUG_FLAG:
            #     model_fit = model.fit(disp=1)
            # else:
            model_fit = model.fit(disp=-1)
            # real_output = interval_list[t:t+seq_length] # for debug
            output = model_fit.forecast()[0]
            yhat = max(0, output)
            if log_input_df.at[t, 'TS_DIFF'] - yhat * off_set > 0:
                # Prediction success
                log_input_df.at[t, 'REQ_TYPE'] = 'p'
                good_cnt += 1
            else:
                # Prediction failed
                log_input_df.at[t, 'REQ_TYPE'] = 'n'

            if len(history) > history_size:
                history.pop(0)
            history.append(log_input_df.at[t, 'TS_DIFF'])

            # if DEBUG_FLAG:
            #     if t % seq_length == 0:
            #         t_end = time.time()
            #         t_elapsed = t_end - t_begin
            #         print("Timestep %d, time elapsed: %.2f" % (t, t_elapsed))
            #         t_begin = t_end
    except:
        print("ARIMA Failed at: ", INPUT_FILE)
        prediction_failure_list.append(INPUT_FILE)

        continue

    try:
        prediction_accuracy = float(good_cnt / tot_cnt * 100)
        prediction_accuracy_dict[INPUT_FILE] = prediction_accuracy
    except:
        print("Invalid file: ", INPUT_FILE)
        prediction_failure_list.append(INPUT_FILE)

    t_end = time.time()
    print("%s accuracy %s, processing time %8.2f sec" %(INPUT_FILE, prediction_accuracy, t_end - t_start))
    # if DEBUG_FLAG:
    #     print("Prediction accuracy: ", prediction_accuracy)

    # DEBUG print DF
    # for i in range(len(log_input_df.index)):
    #     print(log_input_df.at[i])

    ## UNAVCO pattern
    log_input_df.to_csv(OUTPUT_DIR + OUTPUT_FILE, header=None, index=False,
                            cols=['TS', 'USER_ID', 'OBJ_ID', 'SIZE', 'TRANS_TIME', 'REQ_TYPE'])

    ## OOI pattern
    # log_input_df.to_csv(OUTPUT_DIR+OUTPUT_FILE, header=None, index=False, columns=['TS', 'USER_ID', 'OBJ_ID', 'T_START', 'T_END', 'REQ_TYPE']) # PYTHON 3.6+
    log_input_df.to_csv(OUTPUT_DIR + OUTPUT_FILE, header=None, index=False,
                        cols=['TS', 'USER_ID', 'OBJ_ID', 'T_START', 'T_END', 'REQ_TYPE']) # Python 2.6+



######
# Done all ARIMA prediction
######

# Output failure files
with open(STAT_DIR + FAIL_LIST, "w+") as statistics_failure:
    writer_failure = csv.writer(statistics_failure)
    writer_failure.writerows(zip(prediction_failure_list))  # write failed file name into CSV in column

# Output the prediction accuracy rate into file
with open(STAT_DIR+STATISTIC_DOC, "a+") as statistics_file:
    csv_writer2 = csv.writer(statistics_file)
    for file_name, pred_val in prediction_accuracy_dict.items():
        csv_writer2.writerow([file_name, pred_val, time.ctime()]) # filename, prediction accuracy, time







