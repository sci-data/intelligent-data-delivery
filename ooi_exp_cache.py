import pylru
import json
import csv
import collections
import datetime
# import cProfile
import re

# CACHE_SIZE = 131072 # 128GB
# CACHE_SIZE = 262144 # 256GB
# CACHE_SIZE = 524288 # 512GB
# CACHE_SIZE = 1048576 # 1TB
# CACHE_SIZE = 2097152 # 2TB

CACHE_SIZE = 10000


# file size in cache
CACHE_FILE_SIZE = collections.defaultdict()
CACHE_FILE_SIZE = {'D0': 0, 'D1': 0, 'D2': 0, 'D3': 0, 'D4': 0, 'D5': 0, 'D6': 0}

# DEBUG
CACHE_FILE_SIZE_MAX = collections.defaultdict()
CACHE_FILE_SIZE_MIN = collections.defaultdict()

CACHE_FILE_SIZE_MAX = {'D0': -100, 'D1': -100, 'D2': -100, 'D3': -100, 'D4': -100, 'D5': -100, 'D6': -100}
CACHE_FILE_SIZE_MIN = {'D0': 10000000, 'D1': 10000000, 'D2': 10000000, 'D3': 10000000, 'D4': 10000000, \
                       'D5': 10000000, 'D6': 10000000}

counter_evict = 0
counter_delete_cache = 0
counter_delete_cache_list = 0
counter_insert = 0
counter_line = 0  # report status
counter_repeated_obj = 0 # Count the repeated request. Which can be reduced by this cache framework

###############
# Use pylru package to implement LRU cache
# https://pypi.org/project/pylru/
###############
size = 100000  # number of elements in cache
cache_D0 = pylru.lrucache(size)
cache_D1 = pylru.lrucache(size)  # init cache
cache_D2 = pylru.lrucache(size)
cache_D3 = pylru.lrucache(size)
cache_D4 = pylru.lrucache(size)
cache_D5 = pylru.lrucache(size)
cache_D6 = pylru.lrucache(size)
# #
# DATA_DIR = '/data_csv/'
# INPUT_FILE = 'data_proc_ts_sorted.csv'
# OUT_DICT_SUMMARY = 'cache_size_' + str(CACHE_SIZE) + '_performance_dict_2.txt'
# OUT_RESULT = 'cache_size_' + str(CACHE_SIZE) + '_performance_result_2.csv'
# OUT_WARNING = 'cache_size_' + str(CACHE_SIZE) + '_WARNING.txt'
# OUT_REPEATED_COUNTER = 'cache_size_' + str(CACHE_SIZE) + '_repeated_counter_2.txt'
#
# ##########
# # Initiate dict
# #########
# user_dtn_dict_file = open('/data_txt/user_dtn_dict.txt', 'r')
# obj_dtn_dict_file = open('/data_txt/obj_dtn_dict.txt', 'r')
# dtn_to_dtn_throughput_dict_file = open('/data_txt/dtn_to_dtn_throughput_dict.txt', 'r')
#
# obj_DH_dict_file = open('/data_txt/obj_DH_dict_new.txt', 'r')
# prefetchable_user_obj_dict_file = open('/data_txt/prefetchable_user_obj_dict.txt', 'r')
# prefetchable_user_obj_dict_counter_file = open('/data_txt/prefetchable_user_obj_dict_counter.txt', 'r')
# CHECKPOINT_DIR = '/my_code/experiment234/checkpoint/'

DATA_DIR = '/PycharmProjects/ML_Classification/data_csv/'
INPUT_FILE = 'data_proc_ts_1m.csv'
# INPUT_FILE = 'data_sample_test.csv'
OUT_DICT_SUMMARY = 'cache_size_' + str(CACHE_SIZE) + '_performance_dict.txt'
OUT_RESULT = 'cache_size_' + str(CACHE_SIZE) + '_performance_result.csv'
OUT_WARNING = 'cache_size_' + str(CACHE_SIZE) + '_WARNING.txt'
CHECKPOINT_DIR = '/PycharmProjects/ML_Classification/data_txt/checkpoint/'
OUT_REPEATED_COUNTER = 'cache_size_' + str(CACHE_SIZE) + '_repeated_counter_2.txt'

##########
# Initiate dict
#########
user_dtn_dict_file = open('/PycharmProjects/ML_Classification/data_txt/user_dtn_dict.txt', 'r')
obj_dtn_dict_file = open('/PycharmProjects/ML_Classification/data_txt/obj_dtn_dict.txt', 'r')
dtn_to_dtn_throughput_dict_file = open('/PycharmProjects/ML_Classification/data_txt/dtn_to_dtn_throughput_dict.txt', 'r')
obj_DH_dict_file = open('/PycharmProjects/ML_Classification/data_txt/obj_DH_dict_new.txt', 'r') # DH
prefetchable_user_obj_dict_file = open('/PycharmProjects/ML_Classification/data_txt/prefetchable_user_obj_dict.txt', 'r')
prefetchable_user_obj_dict_counter_file = open('/PycharmProjects/ML_Classification/data_txt/prefetchable_user_obj_dict_counter.txt', 'r')

user_dtn_dict = collections.defaultdict()
obj_dtn_dict = collections.defaultdict()
dtn_to_dtn_throughput_dict = collections.defaultdict()

# Experiment 3, data hub
obj_DH_dict = collections.defaultdict()
# Experiment 4, prefetching
prefetchable_user_obj_dict = collections.defaultdict(list)

user_dtn_dict = json.loads(user_dtn_dict_file.read())
obj_dtn_dict = json.loads(obj_dtn_dict_file.read()) # DHT
dtn_to_dtn_throughput_dict = json.loads(dtn_to_dtn_throughput_dict_file.read())
obj_DH_dict = json.loads(obj_DH_dict_file.read())
prefetchable_user_obj_dict = json.loads(prefetchable_user_obj_dict_file.read())
prefetchable_user_obj_dict_counter = json.loads(prefetchable_user_obj_dict_counter_file.read())


# Two dict
##############
# init list with default value [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [total_req_counter, tot_req_time_range, tot_cached_req, \
# tot_cached_req_percentage, tot_data_move_from_repo, tot_data_move_from_other_dtns, tot_data_move_from_myself,\
# tot_data_retrieve_time_native, tot_data_retrieve_time_DH, tot_data_retrieve_time_prefetch]
# tot_cached_req_percentage is the accumulative number of percentage of getting file from cache at each request
##############
user_cache_hit_records = collections.defaultdict(lambda: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])


#################
# Convert user_cache_hit_records to int, save to file
# [total_req_counter, tot_req_time_range (in seconds), tot_cached_req (seconds), tot_cached_req_percentage, \
#  avg_cached_req_percentage (= tot_cached_req_percentage/total_req_counter), \
#  avg_cached_data_percentage (= tot_cached_req/tot_req_time_range),
# tot_data_move_from_repo, tot_data_move_from_other_dtns, tot_data_move_from_myself,\
# tot_data_movement = (tot_data_move_from_repo + tot_data_move_from_other_dtns + tot_data_move_from_myself),
# tot_data_retrieve_time_naive, tot_data_retrieve_time_DH, tot_data_retrieve_time_prefetch，
# tot_data_retrieve_time_wout_cache]
#################

user_cache_hit_summary = collections.defaultdict(list)

###################
#     #1 LRU_DS_other_DTN = 0 # Size of data move from other DTN under LRU policy
#     #2 LRU_DS_self_DTN = 0 # Size of data move from self DTN under LRU policy
#     #3 LRU_DS_DR = 0 # Size of data move from data repository under LRU policy
#     #4 DH_DS_other_DTN = 0 # Size of data move from other DTN under Data hub policy
#     #5 DH_DS_self_DTN = 0 # Size of data move from self DTN under Data Hub policy
#     #6 DH_DS_DR = 0
#     #7 PREF_DS_other_DTN = 0 # Size of data move from other DTN under Prefetching policy
#     #8 PREF_DS_self_DTN = 0 # Size of data move from self DTN under Prefetching policy
#     #9 PREF_DS_DR = 0
#     #10 Total number of requests
#     #11 COUNTER_LRU_other_DTN
#     #12 COUNTER_LRU_local_DTN
#     #13 COUNTER_DH_other_DTN
#     #14 COUNTER_DH_local_DTN
#     #15 COUNTER_PREF_other_DTN
#     #16 COUNTER_PREF_local_DTN
##################


user_data_movement_records = collections.defaultdict(lambda: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])


# Obj and its cache_list+++++++++++
OBJ_CACHE_DICT_D0 = collections.defaultdict(list)
OBJ_CACHE_DICT_D1 = collections.defaultdict(list)
OBJ_CACHE_DICT_D2 = collections.defaultdict(list)
OBJ_CACHE_DICT_D3 = collections.defaultdict(list)
OBJ_CACHE_DICT_D4 = collections.defaultdict(list)
OBJ_CACHE_DICT_D5 = collections.defaultdict(list)
OBJ_CACHE_DICT_D6 = collections.defaultdict(list)




def parse_list_time_str_to_datetime(date_list):  # for cache: [[], [], []]
    """
    API Deprecated

    :param date_list:
    :return:
    """
    new_date_list = []
    p = re.compile('[- :]')
    for date_pair in date_list:
        new_date_list.append(
            [
                datetime.datetime(*map(int, p.split(date_pair[0]))),
                datetime.datetime(*map(int, p.split(date_pair[1])))
            ]
        )

    return new_date_list


def parse_datetime_to_list_time_str(date_list):
    """
    API Deprecated

    :param date_list:
    :return:
    """
    new_str_list = []

    for date_pair in date_list:
        date0, time_0 = str(date_pair[0]).split(" ")
        date1, time_1 = str(date_pair[1]).split(" ")

        new_str_list.append(
            [
                date0 + ' ' + time_0,
                date1 + ' ' + time_1
            ]
        )

    return new_str_list


def parse_cache_key_to_time_range_str(key_name):
    """
    Parse cache key into obj_id and time_range_list_str
    :param key_name:
    :return: obj and time_range_list_str []
    """
    key_name_split = key_name.split(' # ')
    obj_id = key_name_split[0]
    time_range_start = key_name_split[1]
    time_range_end = key_name_split[2]

    return obj_id, [time_range_start, time_range_end]


def parse_time_range_to_cache_key(obj_id, time_range_str):
    """
    Parse time range list into cache key name
    :param obj_id:
    :param time_range_str: [[]]
    :return: key_name
    """
    try:
        key_name = str(obj_id) + ' # ' + str(time_range_str[0][0]) + ' # ' + str(time_range_str[0][1])
    except:
        print("ERROR: parse_time_range_to_cache_key(), time_range_str=", time_range_str)

    return key_name


def count_time_range(time_range_list):
    """
    Convert time range into time in second

    :param time_range_list: [[start, end], []]
    :return: total time in second
    """
    tot_time = 0.00

    for time_range in time_range_list:
        if time_range != []:
            try:
                tot_time += float(time_range[1] - time_range[0])
            except:
                print("ERROR: count_time_range(), time_range=", time_range)

    return abs(tot_time)


def count_data_size(obj_id, time_range_list):
    """
    Convert time range to data size in MB

    :param time_range_list: [[]]
    :return: data size in MB
    """
    data_density_list = 10  # temporary number. MB/hour
    tot_data_size = 0  # MB

    tot_time = count_time_range(time_range_list)  # in seconds

    tot_data_size = tot_time / 3600 * data_density_list  # convert to hour then times data density

    return tot_data_size

def count_data_transfer_time_naive(DTN_dest_id, DTN_src_id, data_size, obj_id):
    """
    For experiment 2, naive LRU cache
    Count the time of transferring data_size from DTN source to destination
    :param DTN_dest_id:
    :param DTN_src_id:
    :param data_size: in MB
    :param obj_id: Use to find the data hub
    :return: Time in second
    """
    global dtn_to_dtn_throughput_dict
    global obj_DH_dict

    throughput = float(dtn_to_dtn_throughput_dict[str(DTN_src_id) + str(DTN_dest_id)]) * 1024  # GB/s to MB/s
    transfer_time = float(data_size / throughput)

    return transfer_time



def count_data_transfer_time_DH(DTN_dest_id, DTN_src_id, data_size, obj_id):
    """
    For experiment 3, data hub
    Count the time of transferring data_size from DTN source to destination
    This one modify to support transferring from local data hub
    If the DTN_src_id is data repo, then we don't coount from data hub

    UPDATE:
    Since I haven't figured out the way to select the best approach to define the data hub
    I would assume the DTN with largest throughput to the DTN destination.

    :param DTN_dest_id:
    :param DTN_src_id:
    :param data_size: in MB
    :param obj_id: Use to find the data hub
    :return: Time in second
    """
    global dtn_to_dtn_throughput_dict
    global obj_DH_dict

    best_throughput_to_this_DTN = {'D0': 4.948, 'D1': 20.12, 'D2': 11.256, 'D3': 20.192, 'D4': 18.678, 'D5': 20.252, 'D6': 25.679}

    if DTN_src_id == "DR": # from data repo
        throughput = float(dtn_to_dtn_throughput_dict[str(DTN_src_id) + str(DTN_dest_id)]) * 1024  # GB/s to MB/s
        transfer_time = float(data_size / throughput)

    else:
        # # Select best throughput from DR,
        # data_hub = obj_DH_dict[str(obj_id)]
        # throughput = float(max(dtn_to_dtn_throughput_dict[str(data_hub) + str(DTN_dest_id)], # data hub
        #                        dtn_to_dtn_throughput_dict[str(DTN_src_id) + str(DTN_dest_id)], # from source
        #                        dtn_to_dtn_throughput_dict["DR" + str(DTN_dest_id)] # from data repo
        #                        )) * 1024 # GB/s to MB/s

        throughput = float(best_throughput_to_this_DTN[str(DTN_dest_id)]) * 1024

        # throughput = float(dtn_to_dtn_throughput_dict[str(data_hub) + str(DTN_dest_id)]) * 1024  # GB/s to MB/s
        transfer_time = float(data_size / throughput)

    return transfer_time


def count_data_transfer_time_prefetch(DTN_dest_id, DTN_src_id, data_size, obj_id, user_id):
    """
    For experiment 4, data prefetching
    Count the time of transferring data_size from DTN source to destination

    If the user and obj is prefetchable, then, avoid counting the transfer time
    Otherwise, use the experiment 3 (data hub) approach to calculate the transfer time

    :param DTN_dest_id:
    :param DTN_src_id:
    :param data_size: in MB
    :param obj_id: Use to find the data hub and prefetchable table
    :param user_id: Use to find the prefetchable data object
    :return: Time in second
    """
    global dtn_to_dtn_throughput_dict
    global obj_DH_dict
    global prefetchable_user_obj_dict

    best_throughput_to_this_DTN = {'D0': 4.948, 'D1': 20.12, 'D2': 11.256, 'D3': 20.192, 'D4': 18.678, 'D5': 20.252,
                                   'D6': 25.679}


    if str(user_id) in prefetchable_user_obj_dict and str(obj_id) in prefetchable_user_obj_dict[str(user_id)]:
        # If this obj hasn't been called more than 3 times, means we did not recognize its pattern yet
        # Then, use the normal approach to calculate the transfer time
        # This similate learning the access pattern at runtime

        if prefetchable_user_obj_dict_counter[str(user_id)][str(obj_id)] < 3:
            if DTN_src_id == "DR":  # from data repo
                throughput = float(
                    dtn_to_dtn_throughput_dict[str(DTN_src_id) + str(DTN_dest_id)]) * 1024  # GB/s to MB/s
                transfer_time = float(data_size / throughput)

            else:
                # data_hub = obj_DH_dict[str(obj_id)]
                # throughput = float(dtn_to_dtn_throughput_dict[str(data_hub) + str(DTN_dest_id)]) * 1024  # GB/s to MB/s
                throughput = float(best_throughput_to_this_DTN[str(DTN_dest_id)]) * 1024
                transfer_time = float(data_size / throughput)

            prefetchable_user_obj_dict_counter[str(user_id)][str(obj_id)] += 1
            return transfer_time

        else:
            # This obj is prefetchable, assuming it is already in the local DTN
            return 0

    # Otherwise, use the normal Data hub approach
    if DTN_src_id == "DR": # from data repo
        throughput = float(dtn_to_dtn_throughput_dict[str(DTN_src_id) + str(DTN_dest_id)]) * 1024  # GB/s to MB/s
        transfer_time = float(data_size / throughput)

    else:
        # data_hub = obj_DH_dict[str(obj_id)]
        throughput = float(best_throughput_to_this_DTN[str(DTN_dest_id)]) * 1024
        transfer_time = float(data_size / throughput)

    return transfer_time


def search_cache(obj_id, time_range, DTN_id):
    """
    Search cache data, return intersected cache data block.
    It update the cache order, by calling the cache_loc[key] (LRU)


    If the requesting data belongs to a section of cache data block
    We need to retrieve the entire data block
    This would impact the entire data block cache order

    :param obj_id:
    :param time_range: []
    :return: list of cache data block
    """
    if not time_range:
        return []

    intersection_list = []
    obj_cache_dict = globals()['OBJ_CACHE_DICT_' + str(DTN_id)]
    cache_loc = globals()['cache_' + str(DTN_id)]

    try:
        cache_list = obj_cache_dict[str(obj_id)]
    except:
        print("Oh NO: search_cache() 1")


    new_range_start = time_range[0]
    new_range_end = time_range[1]

    for cache_elem in cache_list:
        if new_range_end < cache_elem[0]:
            # Since elements in cache are in sorted order
            # If time_range less than the cache[ind] elem
            # Then, it won't have any intersection with successors
            # So, break and check next element in time_range
            break

        # Intersection
        if new_range_end >= cache_elem[0] and new_range_start <= cache_elem[1]:
            # Only retrieve the intersection part, rather than the entire data block
            intersection_list.append([max(cache_elem[0], new_range_start), min(cache_elem[1], new_range_end)])
            # Update the lru cache cache_dtn_id
            key_name = parse_time_range_to_cache_key(obj_id, [[cache_elem[0], cache_elem[1]]])
            try:
                value = cache_loc[str(key_name)] # Lookup the value and affect the order of the cache
            except:
                print("Oh NO: search_cache() 2")

    if not intersection_list:
        intersection_list.append([])

    return intersection_list


def search_cache_local_DTN(obj_id, time_range, DTN_id):
    """
    Search cache data on local DTN
    It return the actual intersection for the purpose of calculating data size
    It also return the entire intersection data block, for the purpose of deleting the cache data



    :param obj_id:
    :param time_range: []
    :return: actual_intersection_list, intersection_data_block_list
    """
    if not time_range:
        return []

    actual_intersection_list = []
    intersection_data_block_list = []
    obj_cache_dict = globals()['OBJ_CACHE_DICT_' + str(DTN_id)]
    cache_loc = globals()['cache_' + str(DTN_id)]

    try:
        cache_list = obj_cache_dict[str(obj_id)]
    except:
        print("Oh NO: search_cache() 1_SELF")

    new_range_start = time_range[0]
    new_range_end = time_range[1]

    for cache_elem in cache_list:
        if new_range_end < cache_elem[0]:
            # Since elements in cache are in sorted order
            # If time_range less than the cache[ind] elem
            # Then, it won't have any intersection with successors
            # So, break and check next element in time_range
            break

        # Intersection
        if new_range_end >= cache_elem[0] and new_range_start <= cache_elem[1]:
            # Return the actual intersection
            actual_intersection_list.append([max(cache_elem[0], new_range_start), min(cache_elem[1], new_range_end)])
            # Return the entire intersection data block
            intersection_data_block_list.append([cache_elem[0], cache_elem[1]])
            # Update the lru cache cache_dtn_id
            key_name = parse_time_range_to_cache_key(obj_id, [[cache_elem[0], cache_elem[1]]])
            try:
                value = cache_loc[str(key_name)]  # Lookup the value and affect the order of the cache
            except:
                print("Oh NO: search_cache() 2_SELF")

    if not actual_intersection_list:
        actual_intersection_list.append([])

    if not intersection_data_block_list:
        intersection_data_block_list.append([])

    return actual_intersection_list, intersection_data_block_list


def add_time_range(cache_list, time_range):
    # cache: [[], [], []]
    # time_range: []

    #######
    # Assuming the cache and new_range are already sorted
    # Return new_cache
    ######

    stop_ind = 0  # the index of last stop location
    new_range = []  # new time range list after insertion

    new_range_start = time_range[0]
    new_range_end = time_range[1]

    if stop_ind == len(cache_list):
        new_range.append(time_range)
    else:
        for ind in range(stop_ind, len(cache_list)):
            if new_range_start > cache_list[ind][1]:
                # time range larger than cache[ind] section
                new_range.append(cache_list[ind])
                # If time range larger than all cache section
                if ind + 1 == len(cache_list):
                    new_range.append([new_range_start, new_range_end])

            # time range between to cache interval, add time range to new_range
            elif cache_list[ind][0] > new_range_start and cache_list[ind][0] > new_range_end:
                new_range.append([new_range_start, new_range_end])
                stop_ind = ind
                break  # select next time_range
            elif new_range_start <= cache_list[ind][0] and cache_list[ind][0] <= new_range_end <= cache_list[ind][1]:
                new_range.append([new_range_start, max(new_range_end, cache_list[ind][1])])
                stop_ind = ind + 1
                break
            elif new_range_start <= cache_list[ind][0] and cache_list[ind][1] < new_range_end:
                # time_range contain this cache section, do nothing, continue
                if ind + 1 == len(cache_list):
                    new_range.append([new_range_start, new_range_end])
                else:
                    continue
            elif cache_list[ind][0] <= new_range_start and new_range_end <= cache_list[ind][1]:
                # cache section contains the time range, select next time range
                stop_ind = ind
                break
            elif cache_list[ind][0] <= new_range_start and new_range_end > cache_list[ind][1]:
                # Update new_range_start and continues to next cache section
                new_range_start = cache_list[ind][0]
                if ind + 1 == len(cache_list):
                    new_range.append([new_range_start, new_range_end])
                else:
                    continue
            if ind + 1 == len(cache_list):
                stop_ind = ind + 1

    new_range += cache_list[stop_ind:]

    return new_range


def warning(data_name, data_size):
    """
    Write warning info to warning file
    When some large file exist maximum cache space, e.g., requesting 4 years data

    :return:
    """

    # Write dict to output file
    out_dict_file = open(DATA_DIR + OUT_WARNING, 'a+')
    out_dict_file.write(data_name + ',' + str(data_size) + '\n')
    out_dict_file.close()


def insert_data_to_cache(obj_id, time_range, data_name, data_size, DTN_id):
    """
    Insert data to cache

    :param data_name: Key 'obj # start_date start_time # end_date end_time'
    :param data_size: Value
    :param obj_id: Use for updating cache list
    :param time_range: Use for updating cache list, [s, e]
    :param DTN_id: indicate the DTN cache node
    :return: None
    """
    global CACHE_FILE_SIZE
    global CACHE_FILE_SIZE_MAX
    # DEBUG
    global counter_insert

    cache_loc = globals()['cache_' + str(DTN_id)]
    obj_cache_dict = globals()['OBJ_CACHE_DICT_' + str(DTN_id)]

    data_name = str(data_name)
    # update the data object in lru cache of each DTN
    try:
        cache_loc[data_name] = data_size
    except:
        print("Oh NO: insert_data_to_cache()")

    # Update obj cache dict
    cache_list_temp = obj_cache_dict[str(obj_id)]
    cache_list_temp = add_time_range(cache_list_temp, time_range)
    obj_cache_dict[str(obj_id)] = cache_list_temp

    CACHE_FILE_SIZE[str(DTN_id)] += data_size
    # DEBUG
    CACHE_FILE_SIZE_MAX[str(DTN_id)] = max(CACHE_FILE_SIZE[str(DTN_id)], CACHE_FILE_SIZE_MAX[str(DTN_id)])

    if CACHE_FILE_SIZE[str(DTN_id)] > CACHE_SIZE:
        warning(data_name, data_size)
    counter_insert += 1


def subtract_time_ranges(ranges_1, ranges_2, step=1):
    """
    Subtract ranges of list_of_ranges_2 from ranges of ranges_1 and create new
    list of ranges that contains ranges which fits in ranges of ranges_1 but can
    not be included in ranges from ranges_2.

    Both list of ranges have to be sorted and they can not have ranges that
    'touch' each other (so ranges (3,5) and (6,8) must be represented as one
    range: (3,8)). So combine_ranges function should be used on those lists.

    Examples:
    subtract_ranges(((3,5), (7,10), (12,16), (20,25), (28,29)), ((0,2), (6,8), (13,18), (22,23), (26,30)))
    [(3, 5), (9, 10), (12, 12), (20, 21), (24, 25)]
    subtract_ranges(((3,5), (7,10)), [])
    [(3, 5), (7, 10)]

    Arguments:
    ranges_1,
    ranges_2 -- iterable containing elements that can be 0 and 1 indexed
                (element[0] and element[1] must raise no exception)
    step     -- step in range or minimal distance between two elements
                It is 1 for integers and for instance for dates should be
                datetime.timedelta(days=1).

    Code source:
    https://gist.github.com/barszczmm/8447665

    """
    # if second list is empty first should be returned without any change
    if not ranges_2:
        # return copy of input list
        return list(ranges_1)

    new_ranges = []

    length = len(ranges_2)

    for range_1 in ranges_1:
        new_range_start = range_1[0]
        new_range_end = range_1[1]

        for i, range_2 in enumerate(ranges_2):

            if any(range_2):
                try:
                    range_2[0] = int(range_2[0])
                    range_2[1] = int(range_2[1])
                    # ignore range_2 if it is before new range item
                    if range_2[1] < new_range_start:
                        if i + 1 == length:
                            new_ranges.append([new_range_start, new_range_end])
                        else:
                            continue

                    # ignore range_2 (and all further) if it is after new range item
                    if range_2[0] > new_range_end:
                        new_ranges.append([new_range_start, new_range_end])
                        break

                    # new range item and range_2 intersects
                    if range_2[1] >= new_range_start \
                            and range_2[0] <= new_range_end:

                        # no new range item if range_2 contains new range item
                        if range_2[0] <= new_range_start \
                                and range_2[1] >= new_range_end:
                            break  # Not add anything to new_ranges list, is deleting

                        # range_2 'cuts' beginning of new range item
                        if range_2[0] < new_range_start:
                            new_range_start = range_2[1] + step

                        # range_2 'cuts' end of new range item
                        if range_2[1] > new_range_end:
                            new_range_end = range_2[0] - step

                        # new range item contains range_2
                        if range_2[0] >= new_range_start and range_2[1] <= new_range_end:
                            new_ranges.append([new_range_start, range_2[0] - step])
                            new_range_start = range_2[1] + step
                            if new_range_start > new_range_end:
                                break
                            # else:
                            #     new_ranges.append([new_range_start, new_range_end])

                        if i + 1 == length:
                            new_ranges.append([new_range_start, new_range_end])
                except:
                    print("ERROR ranges_1, ranges_2:", ranges_1, ranges_2)

    return new_ranges


def delete_cache_list(obj_id, time_range_list, DTN_id):
    """
    Delete time range list from object cache list
    :param obj_id:
    :param time_range_list: [[], []]
    :return: None
    """

    # DEBUG
    global counter_delete_cache_list
    counter_delete_cache_list += 1

    obj_cache_dict = globals()['OBJ_CACHE_DICT_' + str(DTN_id)]

    cache_list_tmp = obj_cache_dict[str(obj_id)]
    cache_list_tmp = subtract_time_ranges(cache_list_tmp, time_range_list)
    obj_cache_dict[str(obj_id)] = cache_list_tmp


def delete_cache_data(obj_id, intersection_list, DTN_id):
    """

    :param obj_id:
    :param intersection_list: [[int start, int end], [s, e]]
    :param DTN_id: indicate DTN cache node
    :return:
    """
    global CACHE_FILE_SIZE
    global CACHE_FILE_SIZE_MIN
    # DEBUG
    global counter_delete_cache
    counter_delete_cache += 1

    cache_loc = globals()['cache_' + str(DTN_id)]

    # Delete from cache
    for date_pair in intersection_list:
        if any(date_pair):
            key_name = parse_time_range_to_cache_key(obj_id, [date_pair])

            # DEBUG Check if this key in the cache
            try:
                if key_name in cache_loc:
                    del cache_loc[key_name]
                else:
                    print("ERROR, delete_cache_data(): " + key_name + ' not in cache')
            except:
                print("Oh NO: delete_cache_data()")

    # Delete them from cache list
    delete_cache_list(obj_id, intersection_list, DTN_id)

    # Update cache file size
    deleted_data_size = count_data_size(obj_id, intersection_list)
    CACHE_FILE_SIZE[str(DTN_id)] -= deleted_data_size  # Update cache
    # DEBUG
    CACHE_FILE_SIZE_MIN[str(DTN_id)] = min(CACHE_FILE_SIZE_MIN[str(DTN_id)], CACHE_FILE_SIZE[str(DTN_id)])


def evict_cache(target_space, DTN_id):
    """
    Evict cache and till it has target space

    Delete object in cache and respective cache_list
    :param: DTN_id
    :return:
    """
    global CACHE_FILE_SIZE
    global CACHE_FILE_SIZE_MIN
    # DEBUG
    global counter_evict
    key_list = []
    key_size = 0

    cache_loc = globals()['cache_' + str(DTN_id)]

    try:
        key_list = list(cache_loc.keys())  # key list from most recently used to least recently used
    except:
        print('Oh No: evict_cache() 1')

    del_key_list = []

    for key in reversed(key_list):
        del_key_list.append(key)
        try:
            key_size = cache_loc.peek(key)
        except:
            print('Oh No: evict_cache() 2')

        if CACHE_SIZE - (CACHE_FILE_SIZE[str(DTN_id)] - key_size) < target_space:
            CACHE_FILE_SIZE[str(DTN_id)] -= key_size
            # DEBUG
            CACHE_FILE_SIZE_MIN[str(DTN_id)] = min(CACHE_FILE_SIZE_MIN[str(DTN_id)], CACHE_FILE_SIZE[str(DTN_id)])
            continue
        else:
            CACHE_FILE_SIZE[str(DTN_id)] -= key_size
            # DEBUG
            CACHE_FILE_SIZE_MIN[str(DTN_id)] = min(CACHE_FILE_SIZE_MIN[str(DTN_id)], CACHE_FILE_SIZE[str(DTN_id)])
            break

    # Evict the last few keys in del_key_list
    for key in del_key_list:
        # Delete from cache
        try:
            del cache_loc[key]
        except:
            print('Oh No: evict_cache() 3')

        # Delete from cache list
        obj_id, time_range_list_str = parse_cache_key_to_time_range_str(key)
        delete_cache_list(obj_id, [time_range_list_str], DTN_id)

        # DEBUG
        counter_evict += 1



def retrieve_and_cache_operation(request_start, request_end, user_id, obj_id, user_self_DTN, obj_DTN_list):
    """
    This function do the data retrieval and cache operation
    :param request_start:
    :param request_end:
    :param user_id:
    :param obj_id:
    :param user_self_DTN: user itself DTN id
    :param obj_DTN_list: The list of DTNs which contains the obj
    :return: None
    """
    # Declare
    global CACHE_SIZE
    global CACHE_FILE_SIZE
    global CACHE_FILE_SIZE_MIN
    global user_cache_hit_records

    global counter_line


    # parse user requesting time range into datetime type list
    # user_req_time_range_list = parse_list_time_str_to_datetime([[request_start, request_end]]) # NEED TO REVISE
    user_req_time_range_list = [[request_start, request_end]]
    user_req_time_range_list_org = [[request_start, request_end]] # origin copy

    # Calculate user requesting time range in seconds
    user_req_time_range_num = count_time_range(user_req_time_range_list)

    # Count the size of user requested time range
    user_req_data_size = count_data_size(obj_id, user_req_time_range_list)

    cache_data_intersection = [[]]
    cache_data_intersection_from_selfDTN = [[]] # for calculating the actual data size
    cache_data_intersection_from_selfDTN_entire_data_block = [[]] # For updating local cache
    data_size_from_other_dtns = 0
    data_size_from_myself = 0
    data_size_from_repo = 0
    tot_data_transfer_time_naive = float(0)  # experiment 2
    tot_data_transfer_time_DH = float(0)  # experiment 3
    tot_data_transfer_time_prefetch = float(0)  # experiment 4


    # Measure data movement
    LRU_DS_other_DTN = 0 # Size of data move from other DTN under LRU policy
    LRU_DS_self_DTN = 0 # Size of data move from self DTN under LRU policy
    DH_DS_other_DTN = 0 # Size of data move from other DTN under Data hub policy
    DH_DS_self_DTN = 0 # Size of data move from self DTN under Data Hub policy
    PREF_DS_other_DTN = 0 # Size of data move from other DTN under Prefetching policy
    PREF_DS_self_DTN = 0 # Size of data move from self DTN under Prefetching policy



    for DTN_id in obj_DTN_list:

        if DTN_id != user_self_DTN:
            # Check if transfer speed less than from DR, then get it from DR
            if dtn_to_dtn_throughput_dict[str(DTN_id) + str(user_self_DTN)] < dtn_to_dtn_throughput_dict[
                'DR' + str(user_self_DTN)]:
                # Skip looking up this DTN
                # print("Getting from %s to %s slower than getting from Data Repo!" % (str(DTN_id), str(user_self_DTN)))
                continue
            else:

                # Find all intersections in user requesting time range
                for index in range(len(user_req_time_range_list)):
                    cache_data_intersection += search_cache(str(obj_id), user_req_time_range_list[index], DTN_id)

                if not any(cache_data_intersection):
                    # list is empty
                    # calculate the data movement
                    cache_data_size = 0
                    data_size_from_other_dtns += 0

                    LRU_DS_other_DTN += 0
                    DH_DS_other_DTN += 0
                    PREF_DS_other_DTN += 0

                    tot_data_transfer_time_naive += float(0)
                    tot_data_transfer_time_DH += float(0)
                    tot_data_transfer_time_prefetch += float(0)
                    continue
                else:
                    # calculate the data movement
                    cache_data_size = count_data_size(obj_id, cache_data_intersection)
                    data_size_from_other_dtns += cache_data_size

                    LRU_DS_other_DTN += cache_data_size
                    DH_DS_other_DTN += 0
                    PREF_DS_other_DTN += 0


                    tot_data_transfer_time_naive += float(
                        count_data_transfer_time_naive(user_self_DTN, DTN_id, cache_data_size, obj_id))
                    tot_data_transfer_time_DH += float(
                        count_data_transfer_time_DH(user_self_DTN, DTN_id, cache_data_size, obj_id))
                    tot_data_transfer_time_prefetch += float(
                        count_data_transfer_time_prefetch(user_self_DTN, DTN_id, cache_data_size, obj_id, user_id))


        else:  # getting data from local DTN cache, no cost
            # Find all intersections in user requesting time range
            for index in range(len(user_req_time_range_list)):
                tmp_list_1, tmp_list_2 = search_cache_local_DTN(str(obj_id),
                                                                               user_req_time_range_list[index], DTN_id)
                cache_data_intersection_from_selfDTN += tmp_list_1
                cache_data_intersection_from_selfDTN_entire_data_block += tmp_list_2

            if not any(cache_data_intersection_from_selfDTN):
                data_size_from_myself = 0
                tot_data_transfer_time_naive += float(0)
                tot_data_transfer_time_DH += float(0)
                tot_data_transfer_time_prefetch += float(0)
            else:
                # calculate the data movement
                data_size_from_myself += count_data_size(obj_id, cache_data_intersection_from_selfDTN)
                tot_data_transfer_time_naive += float(0)
                tot_data_transfer_time_DH += float(0)
                tot_data_transfer_time_prefetch += float(0)

        # Update the request range for searching at next DTN
        if any(cache_data_intersection):
            user_req_time_range_list = subtract_time_ranges(user_req_time_range_list, cache_data_intersection)

        if any(cache_data_intersection_from_selfDTN):
            user_req_time_range_list = subtract_time_ranges(user_req_time_range_list,
                                                            cache_data_intersection_from_selfDTN)

    # Getting the rest of request time range from data repository
    data_size_from_repo = count_data_size(obj_id, user_req_time_range_list)
    time_range_to_get_from_repo = count_time_range(user_req_time_range_list)
    tot_data_transfer_time_naive += float(count_data_transfer_time_naive(user_self_DTN, 'DR', data_size_from_repo, obj_id))
    tot_data_transfer_time_DH += float(count_data_transfer_time_DH(user_self_DTN, 'DR', data_size_from_repo, obj_id))
    tot_data_transfer_time_prefetch += float(count_data_transfer_time_prefetch(user_self_DTN, 'DR', data_size_from_repo, obj_id, user_id))





    # Update local cache
    # Need to check if it is too large to fit the cache
    if user_req_data_size <= CACHE_SIZE:
        # Delete the intersection from local DTN and insert new request time range to local DTN
        if any(cache_data_intersection_from_selfDTN_entire_data_block):
            delete_cache_data(obj_id, cache_data_intersection_from_selfDTN_entire_data_block, user_self_DTN)

        if CACHE_FILE_SIZE[str(user_self_DTN)] + user_req_data_size > CACHE_SIZE:
            evict_cache(user_req_data_size, user_self_DTN)  # evict cache till it can store user_req_data_size

        # Store data to cache
        data_name = parse_time_range_to_cache_key(obj_id, user_req_time_range_list_org)
        insert_data_to_cache(obj_id, user_req_time_range_list_org[0], data_name, user_req_data_size, user_self_DTN)

    else: # File is too large, don't store to cache
        # Write to warning log
        data_name = parse_time_range_to_cache_key(obj_id, user_req_time_range_list)
        warning(data_name, user_req_data_size)


    # Record parameters value

    # Experiment 2
    if not any(cache_data_intersection) and not any(cache_data_intersection_from_selfDTN):
        user_cache_hit_records[user_id][0] += 1  # total_req_counter
        user_cache_hit_records[user_id][1] += user_req_time_range_num
        user_cache_hit_records[user_id][2] += 0
        user_cache_hit_records[user_id][3] += 0
        user_cache_hit_records[user_id][4] += data_size_from_repo
        user_cache_hit_records[user_id][5] += data_size_from_other_dtns
        user_cache_hit_records[user_id][6] += data_size_from_myself
        user_cache_hit_records[user_id][7] += float(tot_data_transfer_time_naive)
        user_cache_hit_records[user_id][8] += float(tot_data_transfer_time_DH)
        user_cache_hit_records[user_id][9] += float(tot_data_transfer_time_prefetch)

    else:
        # Calculate parameters
        # user_req_time_range_num = count_time_range(user_req_time_range_list)
        # cached_req_time_range = count_time_range(cache_data_intersection)
        # cached_req_time_range += count_time_range(cache_data_intersection_from_selfDTN)
        cached_req_time_range = user_req_time_range_num - time_range_to_get_from_repo
        if user_req_time_range_num != 0:
            cached_req_perc = cached_req_time_range / user_req_time_range_num  # cached_request_precentage
            # if cached_req_perc > 1:
            #     print("Oh no!")
        else:
            cached_req_perc = 0

        user_cache_hit_records[user_id][0] += 1  # total_req_counter
        user_cache_hit_records[user_id][1] += user_req_time_range_num
        user_cache_hit_records[user_id][2] += cached_req_time_range
        user_cache_hit_records[user_id][3] += cached_req_perc
        user_cache_hit_records[user_id][4] += data_size_from_repo
        user_cache_hit_records[user_id][5] += data_size_from_other_dtns
        user_cache_hit_records[user_id][6] += data_size_from_myself
        user_cache_hit_records[user_id][7] += float(tot_data_transfer_time_naive)
        user_cache_hit_records[user_id][8] += float(tot_data_transfer_time_DH)
        user_cache_hit_records[user_id][9] += float(tot_data_transfer_time_prefetch)

    return


def three_caching_mechanism_operations(request_start, request_end, user_id, obj_id, user_self_DTN, obj_DTN_list):
    """
    Comparing the data movement under the three caching mechanisms
    LRU, LRU+DH, LRU+DH+Prefetching

    :param request_start:
    :param request_end:
    :param user_id:
    :param obj_id:
    :param user_self_DTN:
    :param obj_DTN_list:
    :return:
    """
    # Declare
    global CACHE_SIZE
    global CACHE_FILE_SIZE
    global CACHE_FILE_SIZE_MIN
    global user_cache_hit_records

    global counter_line
    global counter_repeated_obj

    # parse user requesting time range into datetime type list
    # user_req_time_range_list = parse_list_time_str_to_datetime([[request_start, request_end]]) # NEED TO REVISE
    user_req_time_range_list = [[request_start, request_end]]
    user_req_time_range_list_org = [[request_start, request_end]] # origin copy

    org_tr = count_time_range(user_req_time_range_list_org) # original time range

    # Calculate user requesting time range in seconds
    user_req_time_range_num = count_time_range(user_req_time_range_list)

    # Counter the size of user requested time range
    user_req_data_size = count_data_size(obj_id, user_req_time_range_list)


    # Measure data movement
    LRU_DS_other_DTN = 0 # Size of data move from other DTN under LRU policy
    LRU_DS_self_DTN = 0 # Size of data move from self DTN under LRU policy
    LRU_DS_DR = 0 # Size of data move from data repository under LRU policy
    DH_DS_other_DTN = 0 # Size of data move from other DTN under Data hub policy
    DH_DS_self_DTN = 0 # Size of data move from self DTN under Data Hub policy
    DH_DS_DR = 0
    PREF_DS_other_DTN = 0 # Size of data move from other DTN under Prefetching policy
    PREF_DS_self_DTN = 0 # Size of data move from self DTN under Prefetching policy
    PREF_DS_DR = 0

    COUNTER_LRU_other_DTN = 0 # Count the number of request that has cache hit from other DTN
    COUNTER_LRU_local_DTN = 0 # Count the number of request that has cache hit from local DTN
    COUNTER_DH_other_DTN = 0  # Count the number of request that has cache hit from other DTN
    COUNTER_DH_local_DTN = 0  # Count the number of request that has cache hit from local DTN
    COUNTER_PREF_other_DTN = 0  # Count the number of request that has cache hit from other DTN
    COUNTER_PREF_local_DTN = 0  # Count the number of request that has cache hit from local DTN


    cache_data_intersection = [[]]
    cache_data_intersection_from_selfDTN = [[]]  # for calculating the actual data size
    cache_data_intersection_from_selfDTN_entire_data_block = [[]]  # For updating local cache



    for DTN_id in obj_DTN_list:
        if DTN_id != user_self_DTN:
            # Check if transfer speed less than from DR, then get it from DR
            if dtn_to_dtn_throughput_dict[str(DTN_id) + str(user_self_DTN)] < dtn_to_dtn_throughput_dict[
                'DR' + str(user_self_DTN)]:
                None # do nothing
                # Skip looking up this DTN
                # print("Getting from %s to %s slower than getting from Data Repo!" % (str(DTN_id), str(user_self_DTN)))
                # continue
            else:
                # Check out the data block in other DTN cache
                for index in range(len(user_req_time_range_list)):
                    cache_data_intersection += search_cache(str(obj_id), user_req_time_range_list[index], DTN_id)
                    intersection_tr = count_time_range(cache_data_intersection) # time range of the intersection data

                    # Count for repeated requests. If the intersection is equal to the original time range.
                    # Treat it as getting all the requested data from the cache.
                    # This means we don't need to ask repository and thus reduce the number of requests.
                    if intersection_tr == org_tr:
                        counter_repeated_obj += 1

                # Count the data movement under different scenarios
                if not any(cache_data_intersection):
                    LRU_DS_other_DTN += 0
                    LRU_DS_self_DTN += 0

                    DH_DS_other_DTN += 0
                    DH_DS_self_DTN += 0

                    # # Check if this user and the obj is predictable, which means if it has occurred more than 3 times
                    # if str(user_id) in prefetchable_user_obj_dict and str(obj_id) in prefetchable_user_obj_dict[str(user_id)] and prefetchable_user_obj_dict_counter[str(user_id)][str(obj_id)] >= 3: #predictable
                    #     # Prefetchable

                else:
                    cache_data_size = count_data_size(obj_id, cache_data_intersection)
                    LRU_DS_other_DTN += cache_data_size
                    LRU_DS_self_DTN += 0

                    if intersection_tr >= 0.05 * org_tr: # Only when the intersection time range larger than 5% of the original time range
                        COUNTER_LRU_other_DTN = 1
                    else:
                        COUNTER_LRU_other_DTN = 0

                    # Check if current DTN is Data hub for obj_id
                    DH_DTN = obj_DH_dict[str(obj_id)]

                    if DH_DTN == user_self_DTN:  # I am the Data hub
                        # All data is counted as retrieve from local
                        DH_DS_other_DTN += 0
                        DH_DS_self_DTN += cache_data_size
                        if intersection_tr >= 0.05 * org_tr:
                            COUNTER_DH_local_DTN = 1
                        else:
                            COUNTER_DH_local_DTN = 0
                    else:
                        DH_DS_other_DTN += cache_data_size
                        DH_DS_self_DTN += 0
                        if intersection_tr >= 0.05 * org_tr:
                            COUNTER_DH_other_DTN = 1
                        else:
                            COUNTER_DH_other_DTN = 0

                    # Check if it is predictable, otherwise, treat it as getting from other DTN
                    # If it is predictable, do nothing, wait for later
                    # Else, count as the DH
                    if str(user_id) in prefetchable_user_obj_dict and str(obj_id) in prefetchable_user_obj_dict[
                        str(user_id)]:
                        if prefetchable_user_obj_dict_counter[str(user_id)][str(obj_id)] >= 3:  # predictable
                            # Do nothing
                            None
                        else:
                            if DH_DTN == user_self_DTN:  # I am the Data hub
                                # All data is counted as retrieve from local
                                PREF_DS_other_DTN += 0
                                PREF_DS_self_DTN += cache_data_size
                                if intersection_tr >= 0.05 * org_tr:
                                    COUNTER_PREF_local_DTN = 1
                                else:
                                    COUNTER_PREF_local_DTN = 0
                            else:
                                PREF_DS_other_DTN += cache_data_size
                                PREF_DS_self_DTN += 0
                                if intersection_tr >= 0.05 * org_tr:
                                    COUNTER_PREF_other_DTN = 1
                                else:
                                    COUNTER_PREF_other_DTN = 0

                    else: # not prefetchable
                        if DH_DTN == user_self_DTN:  # I am the Data hub
                            # All data is counted as retrieve from local
                            PREF_DS_other_DTN += 0
                            PREF_DS_self_DTN += cache_data_size
                            COUNTER_PREF_local_DTN = 0
                            if intersection_tr >= 0.05 * org_tr:
                                COUNTER_PREF_local_DTN = 1
                        else:
                            PREF_DS_other_DTN += cache_data_size
                            PREF_DS_self_DTN += 0
                            COUNTER_PREF_other_DTN = 0
                            if intersection_tr >= 0.05 * org_tr:
                                COUNTER_PREF_other_DTN = 1


        else: # getting from local DTN
            for index in range(len(user_req_time_range_list)):
                tmp_list_1, tmp_list_2 = search_cache_local_DTN(str(obj_id),user_req_time_range_list[index], DTN_id)
                cache_data_intersection_from_selfDTN += tmp_list_1
                cache_data_intersection_from_selfDTN_entire_data_block += tmp_list_2

                self_intersection_tr = count_time_range(cache_data_intersection_from_selfDTN) # intersection time range

                # Count for the repeated requests
                if self_intersection_tr == org_tr:
                    counter_repeated_obj += 1

            if not any(cache_data_intersection_from_selfDTN):
                LRU_DS_other_DTN += 0
                LRU_DS_self_DTN += 0

                DH_DS_other_DTN += 0
                DH_DS_self_DTN += 0
            else:
                cache_data_size = count_data_size(obj_id, cache_data_intersection_from_selfDTN)
                LRU_DS_other_DTN += 0
                LRU_DS_self_DTN += cache_data_size
                COUNTER_LRU_local_DTN = 0
                if self_intersection_tr >= 0.05 * org_tr:
                    COUNTER_LRU_local_DTN = 1

                DH_DS_other_DTN += 0
                DH_DS_self_DTN += cache_data_size
                COUNTER_DH_local_DTN = 0
                if self_intersection_tr >= 0.05 * org_tr:
                    COUNTER_DH_local_DTN = 1


                # If it is predictable, do nothing, wait for later
                # Else, count as the DH
                if str(user_id) in prefetchable_user_obj_dict and str(obj_id) in prefetchable_user_obj_dict[str(user_id)]:
                    if prefetchable_user_obj_dict_counter[str(user_id)][
                    str(obj_id)] >= 3:  # predictable
                        # Do nothing
                        # Count it in later
                        None
                    else:
                        PREF_DS_other_DTN += 0
                        PREF_DS_self_DTN += cache_data_size
                        COUNTER_PREF_local_DTN = 0
                        if self_intersection_tr >= 0.05 * org_tr:
                            COUNTER_PREF_local_DTN = 1

                else: # not prefetchable
                    PREF_DS_other_DTN += 0
                    PREF_DS_self_DTN += cache_data_size
                    COUNTER_PREF_local_DTN = 0
                    if self_intersection_tr >= 0.05 * org_tr:
                        COUNTER_PREF_local_DTN = 1

        # Update the request range for searching at next DTN
        if any(cache_data_intersection) :
            user_req_time_range_list = subtract_time_ranges(user_req_time_range_list, cache_data_intersection)

        if any(cache_data_intersection_from_selfDTN):
            user_req_time_range_list = subtract_time_ranges(user_req_time_range_list, cache_data_intersection_from_selfDTN)


    # Getting the rest of request time range from data repository
    cache_data_size = count_data_size(obj_id, user_req_time_range_list)
    LRU_DS_DR += cache_data_size
    DH_DS_DR += cache_data_size

    # Checkout if it is predictable
    if str(user_id) in prefetchable_user_obj_dict and str(obj_id) in prefetchable_user_obj_dict[
        str(user_id)]:
        if prefetchable_user_obj_dict_counter[str(user_id)][str(obj_id)] >= 3:  # predictable
            # Count all the data transfer as from local DTN
            cache_data_size_orig = count_data_size(obj_id, user_req_time_range_list_org)
            PREF_DS_self_DTN += cache_data_size_orig
            PREF_DS_other_DTN += 0
            PREF_DS_DR += 0
            prefetchable_user_obj_dict_counter[str(user_id)][str(obj_id)] += 1
            COUNTER_PREF_local_DTN = 1

        else:
            # Update the data transfer from data repository
            PREF_DS_DR += cache_data_size
            PREF_DS_other_DTN += 0
            PREF_DS_self_DTN += 0
            prefetchable_user_obj_dict_counter[str(user_id)][str(obj_id)] += 1

    else: # not prefetchable
        # Update the data transfer from data repository
        PREF_DS_DR += cache_data_size
        PREF_DS_other_DTN += 0
        PREF_DS_self_DTN += 0


    # Update local cache
    # Need to check if it is too large to fit the cache
    if user_req_data_size <= CACHE_SIZE:
        # Delete the intersection from local DTN and insert new request time range to local DTN
        if any(cache_data_intersection_from_selfDTN_entire_data_block):
            delete_cache_data(obj_id, cache_data_intersection_from_selfDTN_entire_data_block, user_self_DTN)

        if CACHE_FILE_SIZE[str(user_self_DTN)] + user_req_data_size > CACHE_SIZE:
            evict_cache(user_req_data_size, user_self_DTN)  # evict cache till it can store user_req_data_size

        # Store data to cache
        data_name = parse_time_range_to_cache_key(obj_id, user_req_time_range_list_org)
        insert_data_to_cache(obj_id, user_req_time_range_list_org[0], data_name, user_req_data_size, user_self_DTN)

    else: # File is too large, don't store to cache
        # Write to warning log
        data_name = parse_time_range_to_cache_key(obj_id, user_req_time_range_list)
        warning(data_name, user_req_data_size)


    # Check out counter
    # If both counter_**_other_dtn and counter_**_local_dtn equal to 1,
    # Then, change to them to 0.5 respectively

    if COUNTER_LRU_local_DTN == 1 and COUNTER_LRU_other_DTN == 1:
        COUNTER_LRU_local_DTN = 0.5
        COUNTER_LRU_other_DTN = 0.5

    if COUNTER_DH_local_DTN == 1 and COUNTER_DH_other_DTN == 1:
        COUNTER_DH_local_DTN = 0.5
        COUNTER_DH_other_DTN = 0.5

    if COUNTER_PREF_local_DTN == 1 and COUNTER_PREF_other_DTN == 1:
        COUNTER_PREF_local_DTN = 0.5
        COUNTER_PREF_other_DTN = 0.5




    # New parameter records
    user_data_movement_records[user_id][0] += LRU_DS_DR
    user_data_movement_records[user_id][1] += LRU_DS_other_DTN
    user_data_movement_records[user_id][2] += LRU_DS_self_DTN
    user_data_movement_records[user_id][3] += DH_DS_DR
    user_data_movement_records[user_id][4] += DH_DS_other_DTN
    user_data_movement_records[user_id][5] += DH_DS_self_DTN
    user_data_movement_records[user_id][6] += PREF_DS_DR
    user_data_movement_records[user_id][7] += PREF_DS_other_DTN
    user_data_movement_records[user_id][8] += PREF_DS_self_DTN
    user_data_movement_records[user_id][9] += 1 # total number of requests
    user_data_movement_records[user_id][10] += COUNTER_LRU_other_DTN
    user_data_movement_records[user_id][11] += COUNTER_LRU_local_DTN
    user_data_movement_records[user_id][12] += COUNTER_DH_other_DTN
    user_data_movement_records[user_id][13] += COUNTER_DH_local_DTN
    user_data_movement_records[user_id][14] += COUNTER_PREF_other_DTN
    user_data_movement_records[user_id][15] += COUNTER_PREF_local_DTN






def checkpoint(counter):
    """
    It dumps the user_cache_hit_records dictionary to file as the checkpoint
    :return:
    """
    with open(CHECKPOINT_DIR+'checkpoint_mem_' + str(CACHE_SIZE) + '_cnt_' + str(counter) + '.txt', 'w+') as checkpoint_file:
        checkpoint_file.write(json.dumps(user_data_movement_records))





def main():
    # Declare
    global CACHE_SIZE
    global CACHE_FILE_SIZE
    global CACHE_FILE_SIZE_MIN
    global user_cache_hit_records
    global counter_line
    global user_dtn_dict
    global obj_dtn_dict
    global counter_repeated_obj

    read_file = open(DATA_DIR + INPUT_FILE, 'r')
    line = read_file.readline()  # CSV header
    line = read_file.readline()  # First line

    while line:
        # Report status
        counter_line += 1
        if counter_line % 100000 == 0:
            print('Line: ', counter_line)

        if counter_line % 1000000 == 0:
            checkpoint(counter_line)

        line_split = line.rstrip().split(',')
        request_start = int(line_split[3])
        request_end = int(line_split[4])
        user_id = line_split[1]
        obj_id = line_split[2]


        user_self_DTN = user_dtn_dict[str(user_id)]
        obj_DTN_list = obj_dtn_dict[str(obj_id)]

        three_caching_mechanism_operations(request_start, request_end, user_id, obj_id, user_self_DTN, obj_DTN_list)

        # Read next line
        line = read_file.readline()

    # Process data
    #
    # for key, val in user_cache_hit_records.items():
    #     user_DTN = user_dtn_dict[str(key)]
    #     throughput_from_DR_to_user_DTN = dtn_to_dtn_throughput_dict['DR'+str(user_DTN)] # use to calculate the transfer time without cache
    #
    #     if val[1] != 0:
    #         user_cache_hit_summary[key] = [val[0], \
    #                                        val[1], \
    #                                        val[2], \
    #                                        val[3], \
    #                                        val[2] / val[1],
    #                                        val[3] / val[0],
    #                                        val[4],
    #                                        val[5],
    #                                        val[6],
    #                                        val[4] + val[5] + val[6],
    #                                        float(val[7]), # transfer_time_naive
    #                                        float(val[8]), # transfer_time_DH
    #                                        float(val[9]),  # transfer_time_prefetch
    #                                        float((val[4] + val[5] + val[6]) / float(throughput_from_DR_to_user_DTN) / 1024), # transfer_time_wout_cache (sec)
    #                                        ]
    #
    #     else:
    #         user_cache_hit_summary[key] = [val[0], \
    #                                        val[1], \
    #                                        val[2], \
    #                                        val[3], \
    #                                        0,
    #                                        val[3] / val[0],
    #                                        val[4],
    #                                        val[5],
    #                                        val[6],
    #                                        val[4] + val[5] + val[6],
    #                                        float(val[7]),  # transfer_time_naive
    #                                        float(val[8]),  # transfer_time_DH
    #                                        float(val[9]),  # transfer_time_prefetch
    #                                        float((val[4] + val[5] + val[6]) / float(throughput_from_DR_to_user_DTN) / 1024),
    #                                        # transfer_time_wout_cache (sec)
    #                                        ]
    #
    # # Write dict to output file
    # out_dict_file = open(DATA_DIR + OUT_DICT_SUMMARY, 'w+')
    # out_dict_file.write(json.dumps(user_cache_hit_summary))
    # out_dict_file.close()

    # Write result to csv file

    header = ['user_id', 'LRU_DS_DR', 'LRU_DS_other_DTN', 'LRU_DS_self_DTN', 'DH_DS_DR', 'DH_DS_other_DTN', \
              'DH_DS_self_DTN', 'PREF_DS_DR', 'PREF_DS_other_DTN', 'PREF_DS_self_DTN', 'COUNTER_tot_request',\
              'COUNTER_LRU_other_DTN', 'COUNTER_LRU_local_DTN', 'COUNTER_DH_other_DTN', 'COUNTER_DH_local_DTN', \
              'COUNTER_PREF_other_DTN', 'COUNTER_PREF_local_DTN']

    with open(DATA_DIR + OUT_RESULT, 'w') as outcsv:
        writer = csv.DictWriter(outcsv, fieldnames=header)
        writer.writeheader()

        writer.writerows({'user_id': int(user_id), \
                          'LRU_DS_DR': val[0], \
                          'LRU_DS_other_DTN': val[1], \
                          'LRU_DS_self_DTN': val[2], \
                          'DH_DS_DR': val[3], \
                          'DH_DS_other_DTN': val[4], \
                          'DH_DS_self_DTN': val[5], \
                          'PREF_DS_DR': val[6], \
                          'PREF_DS_other_DTN': val[7], \
                          'PREF_DS_self_DTN': val[8],\
                          'COUNTER_tot_request': val[9],\
                          'COUNTER_LRU_other_DTN': val[10], \
                          'COUNTER_LRU_local_DTN': val[11], \
                          'COUNTER_DH_other_DTN': val[12], \
                          'COUNTER_DH_local_DTN': val[13],
                          'COUNTER_PREF_other_DTN': val[14], \
                          'COUNTER_PREF_local_DTN': val[15]\
                          } \
                         for user_id, val in user_data_movement_records.items())

    # Output the repeated counter
    with open(DATA_DIR + OUT_REPEATED_COUNTER, 'w+') as out_repeat:
        out_repeat.write(str(counter_repeated_obj))


    # Debug
    for dtn_id in ['D0', 'D1', 'D2', 'D3', 'D4', 'D5', 'D6']:
        print("For DTN ", dtn_id)
        print(' \n')
        cache_loc = globals()['cache_' + dtn_id]
        print("Cache usage:", len(cache_loc))
        print("CACHE_FILE_SIZE:", CACHE_FILE_SIZE[dtn_id])
        print("CACHE_FILE_SIZE_MIN", CACHE_FILE_SIZE_MIN[dtn_id])
        print("CACHE_FILE_SIZE_MAX", CACHE_FILE_SIZE_MAX[dtn_id])
        print(' \n')

    print("counter_insert ", counter_insert)
    print('counter_evict ', counter_evict)
    print('counter_delete_cache ', counter_delete_cache)
    print('counter_delete_cache_list ', counter_delete_cache_list)
    print('Counter_repeated_obj ', counter_repeated_obj)


#########
# Main
########
if __name__ == '__main__':
    # cProfile.run('main()')
    main()
