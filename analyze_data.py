import collections
import json
import datetime
import csv

DATA_DIR = '/PycharmProjects/ML_Classification/data_csv/'
FILE_NAME = 'data_no_0_proc.csv'
obj_dict = collections.defaultdict(list)
obj_dict_proc = collections.defaultdict(list)
DICT_OUT = 'overlap_dict.txt'


# Find common data object
def find_comm_obj_user():
    file = open(DATA_DIR + FILE_NAME, 'r')
    line = file.readline()

    while line:
        user_id = line.split(',')[2]
        obj = line.split(',')[3]
        if user_id not in obj_dict[obj]:
            obj_dict[obj].append(user_id)
        line = file.readline()

    for key, val in obj_dict.items():
        if len(val) >= 2:
            obj_dict_proc[key] = val

    # output overlapped dictionary
    with open(DATA_DIR + DICT_OUT, 'w+') as file:
        file.write(json.dumps(obj_dict_proc))

    # print('length of obj_dict_proc', len(obj_dict_proc.keys()))
    # print(obj_dict_proc)


# Find common data object and user access date
def find_comm_obj_user_date():
    obj_user_date_dict = collections.defaultdict(lambda: collections.defaultdict(list))
    DICT_OUT1 = 'obj_user_date_dict.txt'
    file = open(DATA_DIR + FILE_NAME, 'r')
    line = file.readline()

    while line:
        user_id = line.split(',')[2]
        obj = line.split(',')[3]
        date = line.split(',')[0]
        if user_id not in obj_user_date_dict[obj]:
            obj_user_date_dict[obj][user_id].append(date)
        if date not in obj_user_date_dict[obj][user_id]:
            obj_user_date_dict[obj][user_id].append(date)

        line = file.readline()

    with open(DATA_DIR + DICT_OUT1, 'w+') as file:
        file.write(json.dumps(obj_user_date_dict))


# Find the data access within certain time range
def find_comm_obj_acc_in_range(time_range):
    # Load dictionary
    DICT_OUT2 = 'obj_user_date_dict.txt'
    DICT_OUT3 = 'obj_date_overlap_in_' + str(time_range) + '.txt'
    file = open(DATA_DIR + DICT_OUT2, 'r')
    obj_user_date_dict = json.loads(file.read())
    # obj_date_list = collections.defaultdict(list) # convert all date into a list
    obj_date_candidate = collections.defaultdict(list)  # selected object and its common dates

    # convert time range to datetime type
    t_range = datetime.timedelta(days=time_range)

    for obj_name, user_dict in obj_user_date_dict.items():
        date_list_nest = user_dict.values()
        date_list = [item for sublist in date_list_nest for item in sublist]
        date_candidate_set = set()

        for i in range(len(date_list) - 1):
            date_1 = datetime.datetime.strptime(date_list[i], '%Y-%m-%d').date()
            for j in range(i + 1, len(date_list)):
                date_2 = datetime.datetime.strptime(date_list[j], '%Y-%m-%d').date()
                if abs(date_1 - date_2) <= t_range:
                    date_candidate_set.add(date_list[i])
                    date_candidate_set.add(date_list[j])

        # Add the date_candidate to obj_date_candidate dictionary
        if len(date_candidate_set) != 0:
            obj_date_candidate[obj_name] = list(date_candidate_set)

    # print(len(obj_date_candidate.keys()))
    file2 = open(DATA_DIR + DICT_OUT3, 'w')
    file2.write(json.dumps(obj_date_candidate))


# Find 2 users pair with common objects
def find_2_user_pair_comm_obj(num_comm_obj):
    FILE_NAME1 = 'obj_date_overlap_in_1_day_selected.csv'
    OUT_FILE = '2_user_pair_comm_obj_over_' + str(num_comm_obj) + '.txt'
    file = open(DATA_DIR + FILE_NAME1, 'r')
    user_obj_dict = collections.defaultdict(list)
    user_pair_dict = collections.defaultdict(list)
    line = file.readline()

    while line:
        user_id = line.split(',')[2]
        obj = line.split(',')[3]
        if obj not in user_obj_dict[user_id]:
            user_obj_dict[user_id].append(obj)
        line = file.readline()

    # Find user pairs
    user_list = list(user_obj_dict.keys())

    for i in range(0, len(user_list) - 1):
        set_1 = set(user_obj_dict[user_list[i]])
        for j in range(i + 1, len(user_list)):
            set_2 = set(user_obj_dict[user_list[j]])
            pair = list(set_1 & set_2)

            if len(pair) >= num_comm_obj:
                user_pair_dict[str(user_list[i]) + '-' + str(user_list[j])] = pair

    # output overlapped dictionary
    with open(DATA_DIR + OUT_FILE, 'w') as file:
        file.write(json.dumps(user_pair_dict))


# Find 3 users pair with common objects
def find_3_user_pair_comm_obj(num_comm_obj):
    FILE_NAME2 = '2_user_pair_comm_obj_over_2.txt'
    OUT_FILE = '3_user_pair_comm_obj_over_' + str(num_comm_obj) + '.txt'
    file = open(DATA_DIR + FILE_NAME2, 'r')
    user_pair_dict = collections.defaultdict(list)
    user_pair_dict = json.loads(file.read())

    user_pair_dict2 = collections.defaultdict(list)
    user_pair_dict3 = collections.defaultdict(list)

    # Find user pairs
    user_list = list(user_pair_dict.keys())

    for i in range(0, len(user_list) - 1):
        set_1 = set(user_pair_dict[user_list[i]])
        for j in range(i + 1, len(user_list)):
            set_2 = set(user_pair_dict[user_list[j]])
            pair = list(set_1 & set_2)

            if len(pair) >= num_comm_obj:
                user_pair_dict2[str(user_list[i]) + '-' + str(user_list[j])] = pair

    # Organize dictionary key name
    for key, val in user_pair_dict2.items():
        tmp_key_split_list = sorted(set(key.split('-')))
        key_new = "-".join(str(x) for x in tmp_key_split_list)

        # copy to a new dict
        user_pair_dict3[key_new] = user_pair_dict2[key]
        # old_key_list.append(key)

    # for key in old_key_list:
    #     del user_pair_dict2[key]

    # output overlapped dictionary
    with open(DATA_DIR + OUT_FILE, 'w') as file:
        file.write(json.dumps(user_pair_dict3))


def find_4_user_pair_comm_obj(num_comm_obj):
    FILE_NAME2 = '3_user_pair_comm_obj_over_2.txt'
    OUT_FILE = '4_user_pair_comm_obj_over_' + str(num_comm_obj) + '.txt'
    file = open(DATA_DIR + FILE_NAME2, 'r')
    user_pair_dict = collections.defaultdict(list)
    user_pair_dict = json.loads(file.read())

    user_pair_dict2 = collections.defaultdict(list)
    user_pair_dict3 = collections.defaultdict(list)

    # Find user pairs
    user_list = list(user_pair_dict.keys())

    for i in range(0, len(user_list) - 1):
        set_1 = set(user_pair_dict[user_list[i]])
        for j in range(i + 1, len(user_list)):
            set_2 = set(user_pair_dict[user_list[j]])
            pair = list(set_1 & set_2)

            if len(pair) >= num_comm_obj:
                user_pair_dict2[str(user_list[i]) + '-' + str(user_list[j])] = pair

    # Organize dictionary key name
    for key, val in user_pair_dict2.items():
        tmp_key_split_list = sorted(set(key.split('-')))
        key_new = "-".join(str(x) for x in tmp_key_split_list)

        # copy to a new dict
        user_pair_dict3[key_new] = user_pair_dict2[key]
        # old_key_list.append(key)

    # for key in old_key_list:
    #     del user_pair_dict2[key]

    # output overlapped dictionary
    with open(DATA_DIR + OUT_FILE, 'w') as file:
        file.write(json.dumps(user_pair_dict3))


def parse_list_time_str_to_datetime(date_list):  # for cache: [[], [], []]
    new_date_list = []
    for date_pair in date_list:
        new_date_list.append(
            [
                datetime.datetime.strptime(date_pair[0], '%Y-%m-%d %H:%M:%S'),
                datetime.datetime.strptime(date_pair[1], '%Y-%m-%d %H:%M:%S')
            ]
        )

    return new_date_list


def add_time_range(cache, time_range_list):
    # cache: [[], [], []]
    # time_range_list: [[], []]

    #######
    # Assuming the cache and new_range are already sorted
    # Return new_cache
    ######

    stop_ind = 0  # the index of last stop location
    new_range = []  # new time range list after insertion
    for time_range in time_range_list:
        new_range_start = time_range[0]
        new_range_end = time_range[1]

        if stop_ind == len(cache):
            new_range.append(time_range)
        else:
            for ind in range(stop_ind, len(cache)):
                if new_range_start > cache[ind][1]:
                    # time range larger than cache[ind] section
                    new_range.append(cache[ind])
                    # If time range larger than all cache section
                    if ind + 1 == len(cache):
                        new_range.append([new_range_start, new_range_end])

                # time range between to cache interval, add time range to new_range
                elif cache[ind][0] > new_range_start and cache[ind][0] > new_range_end:
                    new_range.append([new_range_start, new_range_end])
                    stop_ind = ind
                    break  # select next time_range
                elif new_range_start <= cache[ind][0] and cache[ind][0] <= new_range_end <= cache[ind][1]:
                    new_range.append([new_range_start, max(new_range_end, cache[ind][1])])
                    stop_ind = ind + 1
                    break
                elif new_range_start <= cache[ind][0] and cache[ind][1] < new_range_end:
                    # time_range contain this cache section, do nothing, continue
                    if ind + 1 == len(cache):
                        new_range.append([new_range_start, new_range_end])
                    else:
                        continue
                elif cache[ind][0] <= new_range_start and new_range_end <= cache[ind][1]:
                    # cache section contains the time range, select next time range
                    stop_ind = ind
                    break
                elif cache[ind][0] <= new_range_start and new_range_end > cache[ind][1]:
                    # Update new_range_start and continues to next cache section
                    new_range_start = cache[ind][0]
                    if ind + 1 == len(cache):
                        new_range.append([new_range_start, new_range_end])
                    else:
                        continue
                if ind + 1 == len(cache):
                    stop_ind = ind + 1

    new_range += cache[stop_ind:]

    return new_range


# print(add_time_range(parse_str_list_to_datetime(a), parse_str_list_to_datetime(b)))
def subtract_time_ranges(ranges_1, ranges_2, step=1):
    """
    Subtract ranges of list_of_ranges_2 from ranges of ranges_1 and create new
    list of ranges that contains ranges which fits in ranges of ranges_1 but can
    not be included in ranges from ranges_2.

    Both list of ranges have to be sorted and they can not have ranges that
    'touch' each other (so ranges (3,5) and (6,8) must be represented as one
    range: (3,8)). So combine_ranges function should be used on those lists.

    Examples:
    subtract_ranges(((3,5), (7,10), (12,16), (20,25), (28,29)), ((0,2), (6,8), (13,18), (22,23), (26,30)))
    [(3, 5), (9, 10), (12, 12), (20, 21), (24, 25)]
    subtract_ranges(((3,5), (7,10)), [])
    [(3, 5), (7, 10)]

    Arguments:
    ranges_1,
    ranges_2 -- iterable containing elements that can be 0 and 1 indexed
                (element[0] and element[1] must raise no exception)
    step     -- step in range or minimal distance between two elements
                It is 1 for integers and for instance for dates should be
                datetime.timedelta(days=1).

    Code source:
    https://gist.github.com/barszczmm/8447665

    """
    # if second list is empty first should be returned without any change
    if not ranges_2:
        # return copy of input list
        return list(ranges_1)

    new_ranges = []

    length = len(ranges_2)

    for range_1 in ranges_1:
        new_range_start = range_1[0]
        new_range_end = range_1[1]

        for i, range_2 in enumerate(ranges_2):

            # ignore range_2 if it is before new range item
            if range_2[1] < new_range_start:
                if i + 1 == length:
                    new_ranges.append([new_range_start, new_range_end])
                else:
                    continue

            # ignore range_2 (and all further) if it is after new range item
            if range_2[0] > new_range_end:
                new_ranges.append([new_range_start, new_range_end])
                break

            # new range item and range_2 intersects
            if range_2[1] >= new_range_start \
                    and range_2[0] <= new_range_end:

                # no new range item if range_2 contains new range item
                if range_2[0] <= new_range_start \
                        and range_2[1] >= new_range_end:
                    break  # Not add anything to new_ranges list, is deleting

                # range_2 'cuts' beginning of new range item
                if range_2[0] < new_range_start:
                    new_range_start = range_2[1] + step

                # range_2 'cuts' end of new range item
                if range_2[1] > new_range_end:
                    new_range_end = range_2[0] - step

                # new range item contains range_2
                if range_2[0] >= new_range_start and range_2[1] <= new_range_end:
                    new_ranges.append([new_range_start, range_2[0] - step])
                    new_range_start = range_2[1] + step
                    if new_range_start > new_range_end:
                        break
                    # else:
                    #     new_ranges.append([new_range_start, new_range_end])

                if i + 1 == length:
                    new_ranges.append([new_range_start, new_range_end])

    return new_ranges


def find_intersection(cache, time_range):
    ######
    # Return the intersection of time_range to cache
    # cache: [[], []]
    # time_range: [[], []]
    ######
    if not time_range:
        return []

    intersection_list = []

    for time_range_elem in time_range:
        new_range_start = time_range_elem[0]
        new_range_end = time_range_elem[1]
        for ind, cache_elem in enumerate(cache):
            if new_range_end < cache_elem[0]:
                # Since elements in cache are in sorted order
                # If time_range less than the cache[ind] elem
                # Then, it won't have any intersection with successors
                # So, break and check next element in time_range
                break

            # Intersection
            if new_range_end >= cache_elem[0] and new_range_start <= cache_elem[1]:
                intersection_list.append([max(new_range_start, cache_elem[0]), min(new_range_end, cache_elem[1])])

    return intersection_list


def count_time_range(time_range_list):
    ######
    # Return the number of days and hours
    # Assuming the elements in time_range_list are already datetime type
    ######
    tot_time = datetime.timedelta(days=0)

    for time_range in time_range_list:
        tot_time += time_range[1] - time_range[0]

    return tot_time


def cache_performance_by_obj(obj_id):
    FOLDER_DIR = 'per_obj/'
    SOURCE_FILE = 'obj_' + str(obj_id) + '.csv'
    OUT_FILE = 'cache_performance_dict_1.txt'

    cache_list = []
    # init list with default value [0, 0, 0, 0], [total_req_counter, tot_req_time_range, tot_cached_req, tot_cached_req_percentage]
    # tot_cached_req_percentage is the accumulative number of percentage of getting file from cache at each request
    user_cache_hit_records = collections.defaultdict(lambda: [0, datetime.timedelta(seconds=0), \
                                                              datetime.timedelta(seconds=0), 0])

    # Convert user_cache_hit_records to int, save to file
    # [total_req_counter, tot_req_time_range (in seconds), tot_cached_req (seconds), tot_cached_req_percentage, \
    #  avg_cached_req_percentage (= tot_cached_req_percentage/total_req_counter), \
    #  avg_cached_data_percentage (= tot_cached_req/tot_req_time_range)]
    user_cache_hit_summary = collections.defaultdict(list)

    read_file = open(DATA_DIR + FOLDER_DIR + SOURCE_FILE, 'r')
    line = read_file.readline()  # no header

    while line:
        line = line[:-1]  # remove newline character
        line_split = line.split(',')
        request_start = line_split[4] + ' ' + line_split[5]
        request_end = line_split[6] + ' ' + line_split[7]
        user_id = line_split[2]

        # parse user requesting time range into datetime type list
        user_req_time_range_list = parse_list_time_str_to_datetime([[request_start, request_end]])

        # Calculate parameters
        user_req_time_range_num = count_time_range(user_req_time_range_list)
        intersection_list = find_intersection(cache_list, user_req_time_range_list)
        cached_req_time_range = count_time_range(intersection_list)

        if user_req_time_range_num != datetime.timedelta(seconds=0):
            cached_req_perc = cached_req_time_range / user_req_time_range_num  # cached_request_precentage
        else:
            cached_req_perc = 0

        # Record parameters value
        user_cache_hit_records[user_id][0] += 1  # total_req_counter
        user_cache_hit_records[user_id][1] += user_req_time_range_num
        user_cache_hit_records[user_id][2] += cached_req_time_range
        user_cache_hit_records[user_id][3] += cached_req_perc

        # update cache_list
        cache_list = add_time_range(cache_list, user_req_time_range_list)

        line = read_file.readline()

    # Process data
    for key, val in user_cache_hit_records.items():
        if val[1].total_seconds() != 0:
            user_cache_hit_summary[key] = [val[0], \
                                           val[1].total_seconds(), \
                                           val[2].total_seconds(), \
                                           val[3], \
                                           val[3] / val[0], \
                                           val[2].total_seconds() / val[1].total_seconds()]
        else:
            user_cache_hit_summary[key] = [val[0], \
                                           val[1].total_seconds(), \
                                           val[2].total_seconds(), \
                                           val[3], \
                                           val[3] / val[0], \
                                           0]

    out_file = open(DATA_DIR + OUT_FILE, 'a+')
    out_file.write(str(obj_id) + '#' + json.dumps(user_cache_hit_summary) + '\n')  # separate by '#'
    out_file.close()


def analyze_cache_performance():
    ######
    # Iterate each obj file
    # call cache_performance_by_obj() to generate to summary
    #####

    #####
    # Without user 0
    ######

    # obj_list = ['2', '0', '7', '6', '5', '1', '8', '198', '203', '207', '208', '10', '209', '210', '211', '213', '214',
    #             '103', '160', '12', '164', '215', '216', '217', '218', '219', '220', '223', '228', '231', '237', '239',
    #             '258', '261', '268', '273', '281', '285', '289', '292', '298', '302', '331', '358', '365', '367', '372',
    #             '395', '407', '416', '420', '426', '430', '36', '155', '480', '483', '486', '509', '158', '514', '42',
    #             '522', '15', '526', '11', '534', '13', '538', '546', '22', '552', '179', '559', '563', '570', '576',
    #             '583', '585', '591', '595', '606', '612', '622', '629', '686', '688', '692', '695', '699', '700', '701',
    #             '706', '710', '711', '713', '715', '717', '718', '720', '721', '723', '726', '728', '730', '732', '737',
    #             '740', '741', '743', '745', '747', '748', '749', '753', '755', '757', '758', '761', '764', '765', '766',
    #             '767', '769', '770', '772', '773', '775', '776', '777', '778', '779', '780', '782', '783', '784', '785',
    #             '786', '787', '788', '789', '790', '791', '793', '794', '795', '796', '797', '798', '799', '800', '801',
    #             '802', '803', '804', '805', '808', '813', '814', '815', '816', '817', '818', '819', '821', '822', '823',
    #             '824', '825', '826', '827', '828', '829', '830', '831', '833', '834', '835', '837', '841', '844', '845',
    #             '870', '871', '872', '873', '875', '882', '884', '897', '911', '921', '929', '931', '668', '669', '670',
    #             '950', '955', '650', '651', '648', '649', '646', '647', '992', '994', '998', '1002', '1004', '1007',
    #             '1012', '1014', '1019', '1021', '671', '1025', '672', '1028', '673', '1031', '674', '1034', '1037',
    #             '1038', '1042', '1043', '1046', '1048', '1050', '1051', '1053', '1056', '1058', '1061', '1063', '1064',
    #             '1068', '1069', '1073', '1075', '1092', '1103', '1110', '1111', '1116', '1124', '1133', '1140', '1147',
    #             '1148', '1151', '201', '1152', '1155', '1156', '1157', '1160', '1161', '1165', '1167', '1170', '1172',
    #             '1174', '1176', '1178', '1180', '1183', '1184', '1185', '1188', '1190', '1192', '1195', '1197', '1198',
    #             '1201', '1207', '1210', '1211', '1212', '1213', '1214', '1216', '1219', '1221', '1224', '1226', '1228',
    #             '1231', '1233', '663', '664', '1251', '661', '662', '665', '1260', '1262', '666', '1265', '1269',
    #             '1274', '1277', '1278', '1279', '1281', '1286', '1291', '1293', '1295', '1296', '1297', '1299', '1306',
    #             '1311', '1312', '1317', '1323', '1327', '1335', '1338', '1339', '1340', '1345', '1349', '1351', '1353',
    #             '1358', '1364', '1365', '1368', '1376', '1379', '1380', '1382', '1388', '1393', '1395', '1400', '1402',
    #             '1410', '1413', '1414', '1422', '1424', '1435', '1444', '1445', '1448', '1452', '1454', '1457', '1459',
    #             '1462', '1464', '1467', '1470', '1475', '1479', '1482', '1486', '1489', '1492', '1495', '1498', '1500',
    #             '1503', '1507', '1511', '1521', '1522', '1523', '1531', '1537', '652', '653', '654', '655', '656',
    #             '657', '658', '1578', '659', '1581', '660', '1583', '1586', '1589', '1593', '1596', '1598', '1600',
    #             '1603', '1604', '1610', '1614', '1617', '1618', '1622', '1625', '1629', '1632', '463', '1635', '1638',
    #             '1641', '1645', '1648', '1652', '1655', '1656', '1659', '1665', '1666', '1670', '1674', '1678', '1681',
    #             '1687', '1691', '1696', '1700', '1704', '1705', '1708', '1713', '1720', '1728', '1730', '1736', '16',
    #             '1755', '1761', '1764', '1769', '1772', '1775', '1779', '1786', '1789', '1794', '1799', '1802', '1805',
    #             '1806', '1808', '1810', '1811', '1812', '1816', '1819', '143', '1823', '1827', '1830', '1834', '1846',
    #             '1857', '1871', '1872', '1875', '1879', '1886', '1891', '1902', '1931', '2006', '2036', '2040', '199',
    #             '78', '2165', '2207', '2211', '2214', '2215', '2222']

    #####
    # All objects
    ####
    obj_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '22', '29',
                '36', '42', '50', '78', '93', '103', '116', '132', '136', '143', '155', '156', '157', '158', '159',
                '160', '161', '162', '163', '164', '165', '166', '167', '168', '169', '170', '171', '172', '173', '174',
                '175', '176', '177', '178', '179', '180', '181', '182', '183', '184', '185', '186', '187', '188', '189',
                '190', '191', '192', '193', '194', '195', '196', '197', '198', '199', '200', '201', '202', '203', '204',
                '205', '206', '207', '208', '209', '210', '211', '213', '214', '215', '216', '217', '218', '219', '220',
                '221', '222', '223', '224', '225', '226', '227', '228', '229', '230', '231', '232', '233', '234', '235',
                '236', '237', '238', '239', '240', '241', '242', '243', '244', '245', '246', '247', '248', '249', '250',
                '251', '252', '253', '254', '255', '256', '257', '258', '259', '260', '261', '262', '263', '264', '265',
                '266', '267', '268', '269', '270', '271', '272', '273', '274', '275', '276', '277', '278', '279', '280',
                '281', '282', '283', '284', '285', '286', '287', '288', '289', '290', '291', '292', '293', '294', '295',
                '296', '297', '298', '299', '300', '301', '302', '303', '304', '305', '306', '307', '308', '309', '310',
                '311', '312', '313', '314', '315', '316', '317', '318', '319', '320', '321', '322', '323', '324', '325',
                '326', '327', '328', '329', '330', '331', '332', '333', '334', '335', '336', '337', '338', '339', '340',
                '341', '342', '343', '344', '345', '346', '347', '348', '349', '350', '351', '352', '353', '354', '355',
                '356', '357', '358', '359', '360', '361', '362', '363', '364', '365', '366', '367', '368', '369', '370',
                '371', '372', '373', '374', '375', '376', '377', '378', '379', '380', '381', '382', '383', '384', '385',
                '386', '387', '388', '389', '390', '391', '392', '393', '394', '395', '396', '397', '398', '399', '400',
                '401', '402', '403', '404', '405', '406', '407', '408', '409', '410', '411', '412', '413', '414', '415',
                '416', '417', '418', '419', '420', '421', '422', '423', '424', '425', '426', '427', '428', '429', '430',
                '431', '432', '433', '434', '435', '436', '437', '438', '439', '440', '441', '442', '443', '444', '445',
                '446', '447', '448', '449', '450', '451', '452', '453', '454', '455', '456', '457', '458', '459', '460',
                '461', '462', '463', '464', '465', '466', '467', '468', '469', '470', '471', '472', '473', '474', '475',
                '476', '477', '478', '479', '480', '481', '482', '483', '484', '485', '486', '487', '488', '489', '490',
                '491', '492', '493', '494', '495', '496', '497', '498', '499', '500', '501', '502', '503', '504', '505',
                '506', '507', '508', '509', '510', '511', '512', '513', '514', '515', '516', '517', '518', '519', '520',
                '521', '522', '523', '524', '525', '526', '527', '528', '529', '530', '531', '532', '533', '534', '535',
                '536', '537', '538', '539', '540', '541', '542', '543', '544', '545', '546', '547', '548', '549', '550',
                '551', '552', '553', '554', '555', '556', '557', '558', '559', '560', '561', '562', '563', '564', '565',
                '566', '567', '568', '569', '570', '571', '572', '573', '574', '575', '576', '577', '578', '579', '580',
                '581', '582', '583', '584', '585', '586', '587', '588', '589', '590', '591', '592', '593', '594', '595',
                '596', '597', '598', '599', '600', '601', '602', '603', '604', '605', '606', '607', '608', '609', '610',
                '611', '612', '613', '614', '615', '616', '617', '618', '619', '620', '621', '622', '623', '624', '625',
                '626', '627', '628', '629', '630', '631', '632', '633', '634', '635', '636', '637', '638', '639', '640',
                '641', '642', '643', '644', '645', '646', '647', '648', '649', '650', '651', '652', '653', '654', '655',
                '656', '657', '658', '659', '660', '661', '662', '663', '664', '665', '666', '667', '668', '669', '670',
                '671', '672', '673', '674', '675', '676', '677', '678', '679', '680', '681', '682', '683', '684', '685',
                '686', '687', '688', '689', '690', '691', '692', '693', '694', '695', '696', '697', '698', '699', '700',
                '701', '702', '703', '704', '705', '706', '707', '708', '709', '710', '711', '712', '713', '714', '715',
                '716', '717', '718', '719', '720', '721', '722', '723', '724', '725', '726', '727', '728', '729', '730',
                '731', '732', '733', '734', '735', '736', '737', '738', '739', '740', '741', '742', '743', '744', '745',
                '746', '747', '748', '749', '750', '751', '752', '753', '754', '755', '756', '757', '758', '759', '760',
                '761', '762', '763', '764', '765', '766', '767', '768', '769', '770', '771', '772', '773', '774', '775',
                '776', '777', '778', '779', '780', '781', '782', '783', '784', '785', '786', '787', '788', '789', '790',
                '791', '792', '793', '794', '795', '796', '797', '798', '799', '800', '801', '802', '803', '804', '805',
                '806', '807', '808', '809', '810', '811', '812', '813', '814', '815', '816', '817', '818', '819', '820',
                '821', '822', '823', '824', '825', '826', '827', '828', '829', '830', '831', '832', '833', '834', '835',
                '836', '837', '838', '839', '840', '841', '842', '843', '844', '845', '846', '847', '848', '849', '850',
                '851', '852', '853', '854', '855', '856', '857', '858', '859', '860', '861', '862', '863', '864', '865',
                '866', '867', '868', '869', '870', '871', '872', '873', '874', '875', '876', '877', '878', '879', '880',
                '881', '882', '883', '884', '885', '886', '887', '888', '889', '890', '891', '892', '893', '894', '895',
                '896', '897', '898', '899', '900', '901', '902', '903', '904', '905', '906', '907', '908', '909', '910',
                '911', '912', '913', '914', '915', '916', '917', '918', '919', '920', '921', '922', '923', '924', '925',
                '926', '927', '928', '929', '930', '931', '932', '933', '934', '935', '936', '937', '938', '939', '940',
                '941', '942', '943', '944', '945', '946', '947', '948', '949', '950', '951', '952', '953', '954', '955',
                '956', '957', '958', '959', '960', '961', '962', '963', '964', '965', '966', '967', '968', '969', '970',
                '971', '972', '973', '974', '975', '976', '977', '978', '979', '980', '981', '982', '983', '984', '985',
                '986', '987', '988', '989', '990', '991', '992', '993', '994', '995', '996', '997', '998', '999',
                '1000', '1001', '1002', '1003', '1004', '1005', '1006', '1007', '1008', '1009', '1010', '1011', '1012',
                '1013', '1014', '1015', '1016', '1017', '1018', '1019', '1020', '1021', '1022', '1023', '1024', '1025',
                '1026', '1027', '1028', '1029', '1030', '1031', '1032', '1033', '1034', '1035', '1036', '1037', '1038',
                '1039', '1040', '1041', '1042', '1043', '1044', '1045', '1046', '1047', '1048', '1049', '1050', '1051',
                '1052', '1053', '1054', '1055', '1056', '1057', '1058', '1059', '1060', '1061', '1062', '1063', '1064',
                '1065', '1066', '1067', '1068', '1069', '1070', '1071', '1072', '1073', '1074', '1075', '1076', '1077',
                '1078', '1079', '1080', '1081', '1082', '1083', '1084', '1085', '1086', '1087', '1088', '1089', '1090',
                '1091', '1092', '1093', '1094', '1095', '1096', '1097', '1098', '1099', '1100', '1101', '1102', '1103',
                '1104', '1105', '1106', '1107', '1108', '1109', '1110', '1111', '1112', '1113', '1114', '1115', '1116',
                '1117', '1118', '1119', '1120', '1121', '1122', '1123', '1124', '1125', '1126', '1127', '1128', '1129',
                '1130', '1131', '1132', '1133', '1134', '1135', '1136', '1137', '1138', '1139', '1140', '1141', '1142',
                '1143', '1144', '1145', '1146', '1147', '1148', '1149', '1150', '1151', '1152', '1153', '1154', '1155',
                '1156', '1157', '1158', '1159', '1160', '1161', '1162', '1163', '1164', '1165', '1166', '1167', '1168',
                '1169', '1170', '1171', '1172', '1173', '1174', '1175', '1176', '1177', '1178', '1179', '1180', '1181',
                '1182', '1183', '1184', '1185', '1186', '1187', '1188', '1189', '1190', '1191', '1192', '1193', '1194',
                '1195', '1196', '1197', '1198', '1199', '1200', '1201', '1202', '1203', '1204', '1205', '1206', '1207',
                '1208', '1209', '1210', '1211', '1212', '1213', '1214', '1215', '1216', '1217', '1218', '1219', '1220',
                '1221', '1222', '1223', '1224', '1225', '1226', '1227', '1228', '1229', '1230', '1231', '1232', '1233',
                '1234', '1235', '1236', '1237', '1238', '1239', '1240', '1241', '1242', '1243', '1244', '1245', '1246',
                '1247', '1248', '1249', '1250', '1251', '1252', '1253', '1254', '1255', '1256', '1257', '1258', '1259',
                '1260', '1261', '1262', '1263', '1264', '1265', '1266', '1267', '1268', '1269', '1270', '1271', '1272',
                '1273', '1274', '1275', '1276', '1277', '1278', '1279', '1280', '1281', '1282', '1283', '1284', '1285',
                '1286', '1287', '1288', '1289', '1290', '1291', '1292', '1293', '1294', '1295', '1296', '1297', '1298',
                '1299', '1300', '1301', '1302', '1303', '1304', '1305', '1306', '1307', '1308', '1309', '1310', '1311',
                '1312', '1313', '1314', '1315', '1316', '1317', '1318', '1319', '1320', '1321', '1322', '1323', '1324',
                '1325', '1326', '1327', '1328', '1329', '1330', '1331', '1332', '1333', '1334', '1335', '1336', '1337',
                '1338', '1339', '1340', '1341', '1342', '1343', '1344', '1345', '1346', '1347', '1348', '1349', '1350',
                '1351', '1352', '1353', '1354', '1355', '1356', '1357', '1358', '1359', '1360', '1361', '1362', '1363',
                '1364', '1365', '1366', '1367', '1368', '1369', '1370', '1371', '1372', '1373', '1374', '1375', '1376',
                '1377', '1378', '1379', '1380', '1381', '1382', '1383', '1384', '1385', '1386', '1387', '1388', '1389',
                '1390', '1391', '1392', '1393', '1394', '1395', '1396', '1397', '1398', '1399', '1400', '1401', '1402',
                '1403', '1404', '1405', '1406', '1407', '1408', '1409', '1410', '1411', '1412', '1413', '1414', '1415',
                '1416', '1417', '1418', '1419', '1420', '1421', '1422', '1423', '1424', '1425', '1426', '1427', '1428',
                '1429', '1430', '1431', '1432', '1433', '1434', '1435', '1436', '1437', '1438', '1439', '1440', '1441',
                '1442', '1443', '1444', '1445', '1446', '1447', '1448', '1449', '1450', '1451', '1452', '1453', '1454',
                '1455', '1456', '1457', '1458', '1459', '1460', '1461', '1462', '1463', '1464', '1465', '1466', '1467',
                '1468', '1469', '1470', '1471', '1472', '1473', '1474', '1475', '1476', '1477', '1478', '1479', '1480',
                '1481', '1482', '1483', '1484', '1485', '1486', '1487', '1488', '1489', '1490', '1491', '1492', '1493',
                '1494', '1495', '1496', '1497', '1498', '1499', '1500', '1501', '1502', '1503', '1504', '1505', '1506',
                '1507', '1508', '1509', '1510', '1511', '1512', '1513', '1514', '1515', '1516', '1517', '1518', '1519',
                '1520', '1521', '1522', '1523', '1524', '1525', '1526', '1527', '1528', '1529', '1530', '1531', '1532',
                '1533', '1534', '1535', '1536', '1537', '1538', '1539', '1540', '1541', '1542', '1543', '1544', '1545',
                '1546', '1547', '1548', '1549', '1550', '1551', '1552', '1553', '1554', '1555', '1556', '1557', '1558',
                '1559', '1560', '1561', '1562', '1563', '1564', '1565', '1566', '1567', '1568', '1569', '1570', '1571',
                '1572', '1573', '1574', '1575', '1576', '1577', '1578', '1579', '1580', '1581', '1582', '1583', '1584',
                '1585', '1586', '1587', '1588', '1589', '1590', '1591', '1592', '1593', '1594', '1595', '1596', '1597',
                '1598', '1599', '1600', '1601', '1602', '1603', '1604', '1605', '1606', '1607', '1608', '1609', '1610',
                '1611', '1612', '1613', '1614', '1615', '1616', '1617', '1618', '1619', '1620', '1621', '1622', '1623',
                '1624', '1625', '1626', '1627', '1628', '1629', '1630', '1631', '1632', '1633', '1634', '1635', '1636',
                '1637', '1638', '1639', '1640', '1641', '1642', '1643', '1644', '1645', '1646', '1647', '1648', '1649',
                '1650', '1651', '1652', '1653', '1654', '1655', '1656', '1657', '1658', '1659', '1660', '1661', '1662',
                '1663', '1664', '1665', '1666', '1667', '1668', '1669', '1670', '1671', '1672', '1673', '1674', '1675',
                '1676', '1677', '1678', '1679', '1680', '1681', '1682', '1683', '1684', '1685', '1686', '1687', '1688',
                '1689', '1690', '1691', '1692', '1693', '1694', '1695', '1696', '1697', '1698', '1699', '1700', '1701',
                '1702', '1703', '1704', '1705', '1706', '1707', '1708', '1709', '1710', '1711', '1712', '1713', '1714',
                '1715', '1716', '1717', '1718', '1719', '1720', '1721', '1722', '1723', '1724', '1725', '1726', '1727',
                '1728', '1729', '1730', '1731', '1732', '1733', '1734', '1735', '1736', '1737', '1738', '1739', '1740',
                '1741', '1742', '1743', '1744', '1745', '1746', '1747', '1748', '1749', '1750', '1751', '1752', '1753',
                '1754', '1755', '1756', '1757', '1758', '1759', '1760', '1761', '1762', '1763', '1764', '1765', '1766',
                '1767', '1768', '1769', '1770', '1771', '1772', '1773', '1774', '1775', '1776', '1777', '1778', '1779',
                '1780', '1781', '1782', '1783', '1784', '1785', '1786', '1787', '1788', '1789', '1790', '1791', '1792',
                '1793', '1794', '1795', '1796', '1797', '1798', '1799', '1800', '1801', '1802', '1803', '1804', '1805',
                '1806', '1807', '1808', '1809', '1810', '1811', '1812', '1813', '1814', '1815', '1816', '1817', '1818',
                '1819', '1820', '1821', '1822', '1823', '1824', '1825', '1826', '1827', '1828', '1829', '1830', '1831',
                '1832', '1833', '1834', '1835', '1836', '1837', '1838', '1839', '1840', '1841', '1842', '1843', '1844',
                '1845', '1846', '1847', '1848', '1849', '1850', '1851', '1852', '1853', '1854', '1855', '1856', '1857',
                '1858', '1859', '1860', '1861', '1862', '1863', '1864', '1865', '1866', '1867', '1868', '1869', '1870',
                '1871', '1872', '1873', '1874', '1875', '1876', '1877', '1878', '1879', '1880', '1881', '1882', '1883',
                '1884', '1885', '1886', '1887', '1888', '1889', '1890', '1891', '1892', '1893', '1894', '1895', '1896',
                '1897', '1898', '1899', '1900', '1901', '1902', '1903', '1904', '1905', '1906', '1907', '1908', '1909',
                '1910', '1911', '1912', '1913', '1914', '1915', '1916', '1917', '1918', '1919', '1920', '1921', '1922',
                '1923', '1924', '1925', '1926', '1927', '1928', '1929', '1930', '1931', '1932', '1933', '1934', '1935',
                '1936', '1937', '1938', '1939', '1940', '1941', '1942', '1943', '1944', '1945', '1946', '1947', '1948',
                '1949', '1950', '1951', '1952', '1953', '1954', '1955', '1956', '1957', '1958', '1959', '1960', '1961',
                '1962', '1963', '1964', '1965', '1966', '1967', '1968', '1969', '1970', '1971', '1972', '1973', '1974',
                '1975', '1976', '1977', '1978', '1979', '1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987',
                '1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998', '1999', '2000',
                '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013',
                '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026',
                '2027', '2028', '2029', '2030', '2031', '2032', '2033', '2034', '2035', '2036', '2037', '2038', '2039',
                '2040', '2041', '2042', '2043', '2044', '2045', '2046', '2047', '2048', '2049', '2050', '2051', '2052',
                '2053', '2054', '2055', '2056', '2057', '2058', '2059', '2060', '2061', '2062', '2063', '2064', '2065',
                '2066', '2067', '2068', '2069', '2070', '2071', '2072', '2073', '2074', '2075', '2076', '2077', '2078',
                '2079', '2080', '2081', '2082', '2083', '2084', '2085', '2086', '2087', '2088', '2089', '2090', '2091',
                '2092', '2093', '2094', '2095', '2096', '2097', '2098', '2099', '2100', '2101', '2102', '2103', '2104',
                '2105', '2106', '2107', '2108', '2109', '2110', '2111', '2112', '2113', '2114', '2115', '2116', '2117',
                '2118', '2119', '2120', '2121', '2122', '2123', '2124', '2125', '2126', '2127', '2128', '2129', '2130',
                '2131', '2132', '2133', '2134', '2135', '2136', '2137', '2138', '2139', '2140', '2141', '2142', '2143',
                '2144', '2145', '2146', '2147', '2148', '2149', '2150', '2151', '2152', '2153', '2154', '2155', '2156',
                '2157', '2158', '2159', '2160', '2161', '2162', '2163', '2164', '2165', '2166', '2167', '2168', '2169',
                '2170', '2171', '2172', '2173', '2174', '2175', '2176', '2177', '2178', '2179', '2180', '2181', '2182',
                '2183', '2184', '2185', '2186', '2187', '2188', '2189', '2190', '2191', '2192', '2193', '2194', '2195',
                '2196', '2197', '2198', '2199', '2200', '2201', '2202', '2203', '2204', '2205', '2206', '2207', '2208',
                '2209', '2210', '2211', '2212', '2213', '2214', '2215', '2216', '2217', '2218', '2219', '2220', '2221',
                '2222', '2224', '2225', '2294']

    for obj_id in obj_list:
        print('obj_id:', obj_id)
        cache_performance_by_obj(obj_id)


def proc_cache_result():
    '''
    Load data from cache_performance_dict.txt
    Process result from function 'analyze_cache_performance()'
    Get the statistic data for each users
    :return:
    '''
    FILE_NAME_LOC = 'cache_performance_dict.txt'
    tmp_dict = {}
    OUT_FILE = 'cache_performance_result.csv'

    header = ['user_id', 'tot_num_req_obj', 'tot_num_req', 'tot_req_time_range (seconds)', 'tot_cached_req (seconds)',
              'tot_cached_req_percentage', 'avg_cached_data_precentage', 'avg_cached_req_precentage']

    user_statistic_dict = collections.defaultdict(lambda: [0, 0, 0, 0, 0, 0, 0])

    # Load data
    file = open(DATA_DIR + FILE_NAME_LOC, 'r')
    line = file.readline()[:-1]  # remove newline charactors

    while line:

        line_split = line.split('#')
        obj_id = line_split[0]
        tmp_dict = json.loads(line_split[1])

        for user_tmp, val in tmp_dict.items():
            user_statistic_dict[user_tmp][0] += 1  # tot_num_req_obj
            user_statistic_dict[user_tmp][1] += val[0]  # tot_num_req
            user_statistic_dict[user_tmp][2] += val[1]  # tot_req_time_range (seconds)
            user_statistic_dict[user_tmp][3] += val[2]  # tot_cached_req (seconds)
            user_statistic_dict[user_tmp][4] += val[3]  # tot_cached_req_percentage

        line = file.readline()[:-1]

    # debug
    print(len(user_statistic_dict), user_statistic_dict.keys())

    # # Calculate average
    # for user_id, val in user_statistic_dict.items():
    #     if val[2] != 0:
    #         user_statistic_dict[user_tmp][5] = val[3] / val[2]
    #     else:
    #         user_statistic_dict[user_tmp][5] = 0
    #
    #     if val[1] != 0:
    #         user_statistic_dict[user_tmp][6] = val[4] / val[1]
    #     else:
    #         user_statistic_dict[user_tmp][6] = 0
    #
    # print(user_statistic_dict)

    # Output to CSV file
    with open(DATA_DIR + OUT_FILE, 'w') as outcsv:
        writer = csv.DictWriter(outcsv, fieldnames=header)
        writer.writeheader()

        writer.writerows({'user_id': int(user_id), 'tot_num_req_obj': val[0], 'tot_num_req': val[1], \
                          'tot_req_time_range (seconds)': val[2], 'tot_cached_req (seconds)': val[3], \
                          'tot_cached_req_percentage': val[4], 'avg_cached_data_precentage': val[5],
                          'avg_cached_req_precentage': val[6]} \
                         for user_id, val in user_statistic_dict.items())


def find_user_obj_list():
    """
    Find user and its requesting data objects
    :return:
    """
    DATA_TXT_DIR = '/PycharmProjects/ML_Classification/data_txt/'
    OUT_DICT_LOC = 'All_user_obj_dict.txt'
    INPUT_FILE = 'data_sample.csv'

    user_obj_dict = collections.defaultdict(list)

    read_file = open(DATA_DIR + INPUT_FILE, 'r')
    line = read_file.readline()  # header
    line = read_file.readline()  # data

    while line:
        line_strip = line[:-1].split(',')
        user_id = line_strip[2]
        obj_id = line_strip[3]

        if obj_id not in user_obj_dict[user_id]:
            user_obj_dict[user_id].append(obj_id)

        line = read_file.readline()

    output_file = open(DATA_TXT_DIR + OUT_DICT_LOC, 'w+')
    output_file.write(json.dumps(user_obj_dict))


def find_obj_user_list():
    """
    Find objects and user list whom has requested this object
    :return:
    """
    DATA_TXT_DIR = '/PycharmProjects/ML_Classification/data_txt/'
    OUT_DICT_LOC = 'All_obj_user_dict.txt'
    INPUT_FILE = 'All_user_obj_dict.txt'

    user_obj_dict = collections.defaultdict(list)
    obj_user_dict = collections.defaultdict(list)

    # Load input dict
    input_file = open(DATA_TXT_DIR + INPUT_FILE, 'r')
    user_obj_dict = json.loads(input_file.read())

    for user_id, obj_list in user_obj_dict.items():
        for obj_id in obj_list:
            if user_id not in obj_user_dict[obj_id]:
                obj_user_dict[obj_id].append(user_id)

    # DEBUG
    for obj_id, user_list in obj_user_dict.items():
        print(obj_id, user_list)

    output_file = open(DATA_TXT_DIR + OUT_DICT_LOC, 'w+')
    output_file.write(json.dumps(obj_user_dict))


def get_user_userid_dict():
    """
    Get user_id and its USER name
    :return:
    """

    DATA_DIR = '/data_csv/'
    INPUT_FILE = 'data.csv'
    OUTPUT_FILE = 'USER_ID_user_num_dict.txt'

    with open(DATA_DIR + INPUT_FILE, newline='') as datafile:
        # Dictionary to store known parameter
        user = {}
        # Counter: 'userID', 'sensor_data'
        para_counter = [0, 0]

        # Read data
        data_reader = csv.reader(datafile, delimiter=',')

        # Process data
        for row in data_reader:
            user_id = row[2]

            if user_id not in user:
                user[user_id] = para_counter[0]
                # sensor_data_num = para_counter[1]
                para_counter[0] = para_counter[0] + 1

        # output dict_sensor to file
        with open(DATA_DIR + OUTPUT_FILE, 'w+') as file:
            file.write(json.dumps(user))


def convert_sensor_id_to_id_sensor():
    """
    Make the sensor id as the key, sensor name as the value
    Create a new dictionary from sensor_name_id_dict.txt
    :return:
    """

    input_file = open('/PycharmProjects/ML_Classification/data_txt/sensor_name_id_dict.txt', 'r')
    tmp_dict = json.loads(input_file.read())

    sensor_id_name_dict = collections.defaultdict()

    for sensor_name, sensor_id in tmp_dict.items():
        sensor_id_name_dict[sensor_id] = sensor_name

    output = open('/PycharmProjects/ML_Classification/data_txt/sensor_id_name_dict.txt', 'w+')

    output.write(json.dumps(sensor_id_name_dict))


def get_user_site_stream_pair():
    """
    To cluster user
    According to user requesting objects
    Generate (stream, site) pair
    To plot scatter figure
    :return:
    """

    DATA_DIR_LOC = '/PycharmProjects/ML_Classification/data_txt/'

    user_obj_dict = json.loads(open(DATA_DIR_LOC + 'All_user_obj_dict.txt', 'r').read())
    obj_name_dict = json.loads(open(DATA_DIR_LOC + 'sensor_id_name_dict.txt', 'r').read())
    stream_id_dict = json.loads(open(DATA_DIR_LOC + 'stream_num_dict.txt', 'r').read())
    site_id_dict = json.loads(open(DATA_DIR_LOC + 'site_num_list.txt', 'r').read())
    output = open(DATA_DIR_LOC + 'user_stream_site_pair_dict.txt', 'w+')

    user_stream_sit_pair_dict = collections.defaultdict(list)

    for user_id, obj_list in user_obj_dict.items():
        for obj_id in obj_list:
            sensor_refdes = obj_name_dict[obj_id]
            stream_name = sensor_refdes.split('_')[0]
            site_name = sensor_refdes.split('-')[1]

            stream_id = stream_id_dict[stream_name]
            site_id = site_id_dict[site_name]

            site_stream_pair = (str(site_id), str(stream_id))

            if site_stream_pair not in user_stream_sit_pair_dict[user_id]:
                user_stream_sit_pair_dict[user_id].append(site_stream_pair)

    # print(user_stream_sit_pair_dict)

    output_file = output.write(json.dumps(user_stream_sit_pair_dict))


def get_site_coordinate():
    from requests_html import HTMLSession

    URL_PRE = 'https://oceanobservatories.org/site/'
    site_dict = json.loads(
        open('/PycharmProjects/ML_Classification/data_txt/site_num_dict.txt', 'r').read())
    site_corrd_dict = collections.defaultdict()
    session = HTMLSession()
    sel = '#main > article > section > div.fourcol-one.last > div:nth-child(3) > p > br'

    for site_name, site_id in site_dict.items():
        try:
            url = URL_PRE + site_name + '/'
            r = session.get(url)
            res = r.html.find(sel)
            coordination = str(res[0].text)

            # latitude
            if coordination.split(',')[0][-1] == "N":
                lat_cor = coordination.split(',')[0][:-2]
            else:  # "S"
                lat_cor = "-" + coordination.split(',')[0][:-2]

            # Longitude
            if coordination.split(',')[1][-1] == "E":
                long_cor = coordination.split(',')[1][:-2]
            else:  # "W"
                long_cor = "-" + coordination.split(',')[1][1:-2]  # remove leading space

            site_corrd_dict[site_name] = [lat_cor, long_cor]
        except:
            site_corrd_dict[site_name] = ['None']

    # Output
    open('/PycharmProjects/ML_Classification/data_txt/site_corrdination_dict_1.txt', 'w+').write(
        json.dumps(site_corrd_dict))


def convert_coordinate_to_csv():
    cor_dict = json.loads(
        open('/PycharmProjects/ML_Classification/data_txt/site_corrdination_dict.txt', 'r').read())
    # csv_file = open('/PycharmProjects/ML_Classification/data_txt/site_coordinate.csv', 'w+')
    with open('/PycharmProjects/ML_Classification/data_txt/site_coordinate.csv', 'w') as outcsv:
        writers = csv.writer(outcsv)
        writers.writerow(['latitude', 'longitude', 'name', 'color', 'note'])

        for site_name, coor_list in cor_dict.items():
            writers.writerow([coor_list[0], coor_list[1], site_name])


def get_obj_id_site_stream_pair():
    """
    Generate the stream site pair for each data obj
    This will be used for FP-growth algorithm
    :return:
    """

    DATA_DIR_LOC = '/PycharmProjects/ML_Classification/data_txt/'

    user_obj_dict = json.loads(open(DATA_DIR_LOC + 'All_user_obj_dict.txt', 'r').read())
    obj_name_dict = json.loads(open(DATA_DIR_LOC + 'sensor_id_name_dict.txt', 'r').read())
    stream_id_dict = json.loads(open(DATA_DIR_LOC + 'stream_num_dict.txt', 'r').read())
    site_id_dict = json.loads(open(DATA_DIR_LOC + 'site_num_list.txt', 'r').read())
    output = open(DATA_DIR_LOC + 'obj_id_site_stream_pair_dict.txt', 'w+')

    obj_id_stream_sit_pair_dict = collections.defaultdict(list)

    for obj_id, obj_name in obj_name_dict.items():
        stream_name = obj_name.split('_')[0]
        site_name = obj_name.split('-')[1]

        stream_id = stream_id_dict[stream_name]
        site_id = site_id_dict[site_name]

        site_stream_pair = [str(site_id), str(stream_id)]

        obj_id_stream_sit_pair_dict[obj_id] = site_stream_pair;

    output_file = output.write(json.dumps(obj_id_stream_sit_pair_dict))


def track_user_enter_system_timestamp():
    '''
    It track the timestamp of each user's first enter to the system
    :return:
    '''
    input_file = '/PycharmProjects/ML_Classification/data_csv/data_sample_test.csv'
    output_file = '/PycharmProjects/ML_Classification/data_txt/user_enter_system_timestamp.txt'

    user_enter_timestamp = collections.defaultdict()

    input_date = open(input_file, 'r')
    line = input_date.readline()
    line = input_date.readline()

    while line:
        user_id = line.split(',')[1]
        if user_id not in user_enter_timestamp:
            user_enter_timestamp[user_id] = line.split(',')[0]
        line = input_date.readline()

    with open(output_file, 'w+') as file:
        file.write(json.dumps(user_enter_timestamp))


def generate_obj_dtn_dict():
    """
    Generate the obj_dtn dict
    :return:
    """
    obj_user_dict = json.loads(
        open('/PycharmProjects/ML_Classification/data_txt/All_obj_user_dict.txt', 'r').read())

    obj_dtn_dict = collections.defaultdict(set)
    obj_dtn_dict_list = collections.defaultdict(list)

    user_dtn_dict = collections.defaultdict()

    for user_id in range(0, 76):
        user_dtn_dict[str(user_id)] = 'D' + str(user_id % 7)

    for obj_id, user_list in obj_user_dict.items():
        for user_id in user_list:
            obj_dtn_dict[obj_id].add(user_dtn_dict[str(user_id)])

    # Convert set to list
    for obj_id, dtn_list in obj_dtn_dict.items():
        obj_dtn_dict_list[obj_id] = list(dtn_list)

    output_file = open('/PycharmProjects/ML_Classification/data_txt/obj_dtn_dict.txt', 'w+')
    output_file.write(json.dumps(obj_dtn_dict_list))


def generate_dtn_to_dtn_throughput_dict():
    input_file = open('/PycharmProjects/ML_Classification/data_csv/dtn_tmp.csv', 'r')
    output_file = '/PycharmProjects/ML_Classification/data_txt/dtn_to_dtn_throughput_dict.txt'
    line = input_file.read()
    dtn_to_dtn_throughput_dict = collections.defaultdict()

    with open('/PycharmProjects/ML_Classification/data_csv/dtn_tmp.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter='\t')

        for dtn_id_1, line in zip(['DR', 'D0', 'D1', 'D2', 'D3', 'D4', 'D5', 'D6'], csv_reader):
            counter = 0
            for dtn_id_2 in ['DR', 'D0', 'D1', 'D2', 'D3', 'D4', 'D5', 'D6']:
                dtn_to_dtn_throughput_dict[dtn_id_1 + dtn_id_2] = line[counter]
                counter += 1

        print(dtn_to_dtn_throughput_dict)

    with open(output_file, 'w+') as file:
        file.write(json.dumps(dtn_to_dtn_throughput_dict))


def select_data_hub():
    """
    Use obj_dtn group and the throughput of each DTN. Select the one with largest throughput to all the other
    group members as the Data Hub (DH)
    :return:
    """
    import operator

    # user_dtn_dict_file = open('/PycharmProjects/ML_Classification/data_txt/user_dtn_dict.txt', 'r')
    obj_dtn_dict_file = open('/PycharmProjects/ML_Classification/data_txt/obj_dtn_dict.txt', 'r')
    dtn_to_dtn_throughput_dict_file = open(
        '/PycharmProjects/ML_Classification/data_txt/dtn_to_dtn_throughput_dict.txt', 'r')

    # user_dtn_dict = collections.defaultdict()
    obj_dtn_dict = collections.defaultdict()
    dtn_to_dtn_throughput_dict = collections.defaultdict()

    # user_dtn_dict = json.loads(user_dtn_dict_file.read())
    obj_dtn_dict = json.loads(obj_dtn_dict_file.read())  # DHT
    dtn_to_dtn_throughput_dict = json.loads(dtn_to_dtn_throughput_dict_file.read())

    obj_DH_dict = collections.defaultdict()
    tmp_dict = collections.defaultdict()

    for obj_id, dtn_list in obj_dtn_dict.items():
        tmp_dict.clear()
        tot_throughput = 0.00

        for dtn_id_1 in dtn_list:
            for dtn_id_2 in dtn_list:
                if dtn_id_1 != dtn_id_2:
                    tot_throughput += float(dtn_to_dtn_throughput_dict[str(dtn_id_1) + str(dtn_id_2)])
                else:
                    continue

            tmp_dict[str(dtn_id_1)] = tot_throughput

        DH = max(tmp_dict.items(), key=operator.itemgetter(1))[0]
        obj_DH_dict[str(obj_id)] = str(DH)

    with open('/PycharmProjects/ML_Classification/data_txt/obj_DH_dict.txt', 'w+') as file:
        file.write(json.dumps(obj_DH_dict))


def select_data_hub_with_multiple_factors():
    """
    This time, it consider the thruput and user request frequency
    It gives weight to each factor
    :return:
    """

    input_file_dir = '/PycharmProjects/ML_Classification/data_csv/data_proc_ts_1m.csv'
    obj_dtn_hit_counter_dict_file = '/PycharmProjects/ML_Classification/data_txt/obj_dtn_hit_counter_dict.txt'
    obj_dtn_dict_file = open('/PycharmProjects/ML_Classification/data_txt/obj_dtn_dict.txt', 'r')

    user_dtn_dict = json.loads(
        open('/PycharmProjects/ML_Classification/data_txt/user_dtn_dict.txt', 'r').read())
    dtn_to_dtn_throughput_dict = json.loads(
        open('/PycharmProjects/ML_Classification/data_txt/dtn_to_dtn_throughput_dict.txt', 'r').read())
    obj_dtn_dict = json.loads(obj_dtn_dict_file.read())  # DHT

    obj_dtn_hit_counter_dict = collections.defaultdict(lambda: collections.defaultdict(lambda: 0))

    counter = 0

    with open(input_file_dir, 'r') as input_file:
        input_reader = csv.reader(input_file)
        next(input_reader, None)  # skip header

        for row in input_reader:
            # For monitoring the progress
            if counter % 1000000 == 0:
                print('Counter: ', counter)
            counter += 1

            obj_dtn_hit_counter_dict[str(row[2])][user_dtn_dict[str(row[1])]] += 1  # [obj_id][dtn_id]

        # dump file
        with open(obj_dtn_hit_counter_dict_file, 'w+') as obj_file:
            obj_file.write(json.dumps(obj_dtn_hit_counter_dict))

    # Calculate the DH

    weight_thruput = 100
    weight_hit_counter = 1

    obj_DH_dict = collections.defaultdict()
    tmp_dict = collections.defaultdict()  # sort the temp DTN calculation score

    for obj_id, dtn_list in obj_dtn_dict.items():
        tmp_dict.clear()
        tot_throughput = 0.00

        for dtn_id_1 in dtn_list:
            for dtn_id_2 in dtn_list:
                if dtn_id_1 != dtn_id_2:
                    tot_throughput += float(dtn_to_dtn_throughput_dict[str(dtn_id_1) + str(dtn_id_2)])
                else:
                    continue
            try:
                tmp_dict[str(dtn_id_1)] = weight_thruput * tot_throughput + weight_hit_counter * \
                                          obj_dtn_hit_counter_dict[obj_id][dtn_id_1]
            except:
                print("Current obj_id %s, searching DTN_id %s" % (obj_id, dtn_id_1))
                print('The obj_dtn_hit_counter_dict for above obj_id', obj_dtn_hit_counter_dict[obj_id])

        DH = max(tmp_dict.items(), key=operator.itemgetter(1))[0]
        obj_DH_dict[str(obj_id)] = str(DH)

    with open('/PycharmProjects/ML_Classification/data_txt/obj_DH_dict_new.txt', 'w+') as file:
        file.write(json.dumps(obj_DH_dict))


def generate_user_access_interval_and_time_range_pair():
    """
    To analyze the user access pattern, in order to prefetch
    It outputs all time intervals to the dict
    {user1: {obj1: [interval1, interval2, ..., intervalN], obj2: [...] }}
    :return:
    """

    user_obj_call_counter = collections.defaultdict(lambda: collections.defaultdict())
    user_obj_last_access_timestamp = collections.defaultdict(lambda: collections.defaultdict())
    user_obj_access_interval_and_time_range_pair_dict = collections.defaultdict(lambda: collections.defaultdict(set))
    # Use for dumping to txt file
    user_obj_access_interval_and_time_range_pair_dict2 = collections.defaultdict(lambda: collections.defaultdict(list))

    user_obj_call_counter_file = '/PycharmProjects/ML_Classification/data_txt/user_obj_call_counter.txt'
    user_obj_access_interval_and_time_range_pair_dict_file = \
        '/PycharmProjects/ML_Classification/data_txt/user_obj_access_interval_and_time_range_pair_dict.txt'

    with open('/PycharmProjects/ML_Classification/data_csv/data_proc_ts_1m.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader, None)  # skip header

        for line in csv_reader:

            # count the obj
            if line[2] not in user_obj_call_counter[line[1]]:
                user_obj_call_counter[line[1]][line[2]] = 1
            else:
                user_obj_call_counter[line[1]][line[2]] += 1

            # Count time interval
            if line[2] not in user_obj_last_access_timestamp[line[1]]:
                user_obj_last_access_timestamp[line[1]][line[2]] = line[0]  # add the timestamp

            else:
                last_ts = user_obj_last_access_timestamp[line[1]][line[2]]

                access_interval = int(line[0]) - int(last_ts)
                time_length = int(line[4]) - int(line[3])

                user_obj_access_interval_and_time_range_pair_dict[line[1]][line[2]].add((access_interval, time_length))

                user_obj_last_access_timestamp[line[1]][line[2]] = line[0]

    for user_id, obj_list in user_obj_access_interval_and_time_range_pair_dict.items():
        for obj_id, pairs in obj_list.items():
            user_obj_access_interval_and_time_range_pair_dict2[str(user_id)][str(obj_id)] = list(pairs)

    with open(user_obj_call_counter_file, 'w+') as counter_file:
        counter_file.write(json.dumps(user_obj_call_counter))

    with open(user_obj_access_interval_and_time_range_pair_dict_file, 'w+') as pair_file:
        pair_file.write(json.dumps(user_obj_access_interval_and_time_range_pair_dict2))


def sort_csv_file():
    """
    I found out that the data_proc_ts.csv is not sorted in time sequence
    This function sort the csv file by the user access time point
    :return:
    """
    import operator
    import csv

    with open('/PycharmProjects/ML_Classification/data_csv/data_0.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader, None)  # skip header
        sortedlist = sorted(csv_reader, key=operator.itemgetter(0))

    with open('/PycharmProjects/ML_Classification/data_csv/data_0_sorted.csv', 'w+') as csv_file2:
        csv_writer = csv.writer(csv_file2)
        csv_writer.writerow(['access_time', 'userID', 'sensor_data', 'begin_time', 'end_time'])

        for row in sortedlist:
            csv_writer.writerow(row)


def generate_prefetchable_user_obj_dict():
    """
    Find the prefetchable obj_id for each user
    The prefetchable means the access interval larger then 60 sec, otherwise, we treat it as streaming
    And the obj_id has to be fetched more than twice

    注意！！！
    这个分法不严谨！这个function中，只要pair存在超过600的， 就被视为 prefetchable


    :return:
    """
    user_obj_call_counter_file = '/PycharmProjects/ML_Classification/data_txt/user_obj_call_counter.txt'
    user_obj_access_interval_and_time_range_pair_dict_file = \
        '/PycharmProjects/ML_Classification/data_txt/user_obj_access_interval_and_time_range_pair_dict.txt'

    prefetchable_user_obj_dict_file = '/PycharmProjects/ML_Classification/data_txt/prefetchable_user_obj_dict.txt'

    # load dictionary
    user_obj_call_counter = json.loads(open(user_obj_call_counter_file, 'r').read())
    user_obj_access_interval_and_time_range_pair_dict = json.loads(
        open(user_obj_access_interval_and_time_range_pair_dict_file, 'r').read())

    user_obj_pair_filterred_dict = collections.defaultdict(lambda: collections.defaultdict(list))

    prefetchable_user_obj_dict = collections.defaultdict(list)

    # filter interval less than 60 sec
    for user_id, obj_dict in user_obj_access_interval_and_time_range_pair_dict.items():
        for obj_id, pair_list in obj_dict.items():
            for pair in pair_list:
                if pair[0] > 600:
                    user_obj_pair_filterred_dict[str(user_id)][str(obj_id)].append(pair)

    for user_id, obj_dict in user_obj_pair_filterred_dict.items():
        for obj_id, pair in obj_dict.items():
            prefetchable_user_obj_dict[str(user_id)].append(str(obj_id))

    with open(prefetchable_user_obj_dict_file, 'w+') as file:
        file.write(json.dumps(prefetchable_user_obj_dict))


def getting_user_access_sequence_and_obj_hit_count():
    """
    It iterates the input csv file, then it record the user data access sequence, in order to FP-growth mining
    It also records the total number of hit for each object. It is for generate the data hit 2D histogram
    :return:
    """
    input_file_dir = '/PycharmProjects/ML_Classification/data_csv/m2m_user_requests_2.csv'
    user_access_sequence_dict_file = '/PycharmProjects/ML_Classification/data_txt/user_access_sequence_dict_0810.txt'
    obj_hit_counter_dict_file = '/PycharmProjects/ML_Classification/data_txt/obj_hit_counter_dict_0810.txt'

    user_access_sequence_dict = collections.defaultdict(list)
    obj_hit_counter_dict = collections.defaultdict(lambda: 0)

    counter = 0

    with open(input_file_dir, 'r') as input_file:
        input_reader = csv.reader(input_file)
        next(input_reader, None)  # skip header

        for row in input_reader:
            # For monitoring the progress
            if counter % 1000000 == 0:
                print('Counter: ', counter)
            counter += 1

            user_access_sequence_dict[str(row[1])].append(str(row[2]))
            obj_hit_counter_dict[str(row[2])] += 1

    # dump dictionary
    with open(user_access_sequence_dict_file, 'w+') as sequence_file:
        sequence_file.write(json.dumps(user_access_sequence_dict))

    with open(obj_hit_counter_dict_file, 'w+') as obj_file:
        obj_file.write(json.dumps(obj_hit_counter_dict))


def split_input_csv_into_two_parts_by_week():
    """
    Split the one month data into two parts
    1. The first week
    2. The 2nd, 3rd, and 4th week

    In order to use the first data to train the model and use the second one to evaluate the model
    :return:
    """
    with open('/data_csv/data_proc_ts_sorted.csv', 'r') as input_csv:
        with open('/data_csv/data_proc_ts_sorted_1st_week.csv', 'w+') as first_part:
            with open('/data_csv/data_proc_ts_sorted_234_week.csv', 'w+') as second_part:
                input_file = csv.reader(input_csv)
                next(input_file, None)

                output_file_1 = csv.writer(first_part)
                output_file_2 = csv.writer(second_part)

                for line in input_file:
                    if int(line[0]) < 1541635200:  # Nov. 8, 2018, 12:00:00AM
                        output_file_1.writerow(line)

                    else:
                        output_file_2.writerow(line)


def generate_pair_sequence_for_fp_growth():
    res_pair = []
    # tmp_dict = collections.defaultdict(list)
    tmp_dict = collections.defaultdict(set)
    tmp_list = []
    interval = 60
    start_ts = 0
    end_ts = 0
    update_flag = True

    with open('/PycharmProjects/ML_Classification/data_csv/data_proc_ts_1m.csv', 'r') as first_part:
        input_file = csv.reader(first_part)
        next(input_file, None)

        for line in input_file:
            if update_flag:
                start_ts = int(line[0])
                end_ts = int(line[0])
                update_flag = False
            else:
                end_ts = int(line[0])

            if end_ts < start_ts + interval:
                # tmp_dict[str(line[1])].append(int(line[2]))
                tmp_dict[str(line[1])].add(int(line[2]))
            else:
                update_flag = True
                # add to result
                for user_id, obj_set in tmp_dict.items():
                    # tmp_list += [obj_set]
                    tmp_list += [list(obj_set)]

                res_pair += tmp_list
                tmp_list = []
                tmp_dict.clear()

    # print(res_pair)
    # with open('/PycharmProjects/ML_Classification/data_txt/fp_user_input_list.txt', 'w+') as output_file:
    #     output_file.write(json.dumps(res_pair))

    with open('/PycharmProjects/ML_Classification/data_csv/fp_user_input_list.csv', 'w+') as output_file:
        output_csv = csv.writer(output_file)
        for line in res_pair:
            output_csv.writerow(line)


def generate_site_stream_pair_sequence_for_fp_growth():
    """
    Here generate two pairs
    1) for object site
    2) for object stream
    :return:
    """
    res_site_pair = []
    res_stream_pair = []
    # tmp_dict = collections.defaultdict(list)
    tmp_site_dict = collections.defaultdict(set)
    tmp_stream_dict = collections.defaultdict(set)
    tmp_site_list = []
    tmp_stream_list = []
    interval = 1800
    start_ts = 0
    end_ts = 0
    update_flag = True
    site = ''
    stream = ''

    obj_id_site_stream_pair_dict = json.loads(
        open('/PycharmProjects/ML_Classification/data_txt/obj_id_site_stream_pair_dict.txt', 'r').read())

    with open('/PycharmProjects/ML_Classification/data_csv/data_proc_ts_1m.csv', 'r') as first_part:
        # with open('/data_csv/data_proc_ts_sorted_1st_week.csv', 'r') as first_part:
        input_file = csv.reader(first_part)
        next(input_file, None)

        for line in input_file:
            if update_flag:
                start_ts = int(line[0])
                end_ts = int(line[0])
                update_flag = False
            else:
                end_ts = int(line[0])

            if end_ts < start_ts + interval:
                # tmp_dict[str(line[1])].append(int(line[2]))
                site = obj_id_site_stream_pair_dict[str(line[2])][0]
                stream = obj_id_site_stream_pair_dict[str(line[2])][1]
                tmp_site_dict[str(line[1])].add(site)
                tmp_stream_dict[str(line[1])].add(stream)

            else:
                update_flag = True
                # add to result
                for user_id, obj_set in tmp_site_dict.items():
                    # tmp_list += [obj_set]
                    tmp_site_list += [list(obj_set)]

                for user_id, obj_set in tmp_stream_dict.items():
                    # tmp_list += [obj_set]
                    tmp_stream_list += [list(obj_set)]

                res_site_pair += tmp_site_list
                res_stream_pair += tmp_stream_list

                tmp_site_list = []
                tmp_stream_list = []
                tmp_site_dict.clear()
                tmp_stream_dict.clear()

    with open('/PycharmProjects/ML_Classification/data_csv/fp_user_site_list.csv', 'w+') as output_file:
        # with open('/data_csv/fp_user_input_list.csv', 'w+') as output_file:
        output_csv = csv.writer(output_file)
        for line in res_site_pair:
            output_csv.writerow(line)

    with open('/PycharmProjects/ML_Classification/data_csv/fp_user_stream_list.csv',
              'w+') as output_file1:
        # with open('/data_csv/fp_user_input_list.csv', 'w+') as output_file:
        output_csv = csv.writer(output_file1)
        for line in res_stream_pair:
            output_csv.writerow(line)


def generate_obj_sequence_for_fp_growth():
    import collections
    import csv

    interval = 1800  # break to a new line every half hour's record
    start_ts = 0
    end_ts = 0
    update_flag = True
    site = ''
    stream = ''
    tmp_obj_dict = collections.defaultdict(set)
    tmp_obj_list = []
    res_obj_pair = []
    counter_line = 0

    # with open('/PycharmProjects/ML_Classification/data_csv/data_proc_ts_1m.csv', 'r') as first_part:
    with open('/data_csv/data_proc_ts_sorted_1st_week.csv', 'r') as first_part:
        input_file = csv.reader(first_part)
        next(input_file, None)

        for line in input_file:
            counter_line += 1
            if counter_line % 10000 == 0:
                print('Line: ', counter_line)

            if update_flag:
                start_ts = int(line[0])
                end_ts = int(line[0])
                update_flag = False
            else:
                end_ts = int(line[0])

            if end_ts < start_ts + interval:
                obj_id = str(line[2])
                tmp_obj_dict[str(line[1])].add(obj_id)

            else:
                update_flag = True
                for user_id, obj_set in tmp_obj_dict.items():
                    tmp_obj_list += [list(obj_set)]

                res_obj_pair += tmp_obj_list

                tmp_obj_list = []
                tmp_obj_dict.clear()

    # with open('/PycharmProjects/ML_Classification/data_csv/fp_user_obj_list_tmp.csv',
    #           'w+') as output_file1:
    with open('/data_csv/fp_user_obj_list.csv', 'w+') as output_file1:
        output_csv = csv.writer(output_file1)
        n = 10  # break every 10 objects
        for line in res_obj_pair:
            line_final = [line[i * n:(i + 1) * n] for i in range((len(line) + n - 1) // n)]
            for line2 in line_final:
                output_csv.writerow(line2)


def remove_duplicate_rows_from_csv_file():
    reader = open('/PycharmProjects/ML_Classification/data_csv/fp_user_site_list1.csv', 'r')
    # reader = open('/data_csv/fp_user_input_list.csv', 'r')
    lines = reader.read().split('\n')
    reader.close()

    writer = open('/PycharmProjects/ML_Classification/data_csv/fp_user_site_list11111.csv', 'w+')
    # writer = open('/data_csv/fp_user_input_list_clean.csv', 'w+')

    for line in set(lines):
        if line is not '':
            writer.write(line + '\n')
    writer.close()

    # reader = open('/PycharmProjects/ML_Classification/data_csv/fp_user_stream_list.csv', 'r')
    # # reader = open('/data_csv/fp_user_input_list.csv', 'r')
    # lines = reader.read().split('\n')
    # reader.close()
    #
    # writer = open('/PycharmProjects/ML_Classification/data_csv/fp_user_stream_list2.csv', 'w+')
    # # writer = open('/data_csv/fp_user_input_list_clean.csv', 'w+')
    #
    # for line in set(lines):
    #     if line is not '':
    #         writer.write(line + '\n')
    # writer.close()


def clean_fp_model():
    """
    I don't know by what reason, the generated fp model have some rows that confidence larger than 1
    So, I remove them by this function
    :return:
    """
    for supt in [20, 15, 10]:
        for confidence in [1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1]:
            fp_model = json.loads(open(
                '/my_code/fp_growth/site_rules_support_' + str(supt) + '_confidence_' + str(
                    confidence) + '.txt', 'r').read())
            del_index_list = []

            for i in range(len(fp_model)):
                if fp_model[i]['conf'] > 1:
                    del_index_list.append(i)

            for j in reversed(del_index_list):
                del fp_model[j]

            with open('/my_code/fp_growth/clean_model/site_rules_support_' + str(supt) + '_confidence_' + str(
                    confidence) + '.txt', 'w+') as output_file:
                output_file.write(json.dumps(fp_model))


def evaluate_fp_model():
    supt = 20
    confidence = 0.9
    historical_record = []
    historical_record_site = []
    historical_record_stream = []
    site_candidate = []
    stream_candidate = []
    user_cache_hit_counter = 0
    tot_number_of_predicted_obj = 0  # site * stream

    tmp_site_list = []
    tmp_stream_list = []

    tmp_counter_1 = 0
    tmp_counter_2 = 0

    statistics_dict = collections.defaultdict(list)  # total hit, total predicted candidate, object sequence length

    #
    # user_sequence_dict = json.loads(open('/data_txt/user_access_sequence_dict.txt', 'r').read())
    # fp_model_site = json.loads(open(
    #             '/my_code/fp_growth/clean_model/site_rules_support_' + str(supt) + '_confidence_' + str(
    #                 confidence) + '.txt', 'r').read())
    # fp_model_stream = json.loads(open(
    #             '/my_code/fp_growth/clean_model/stream_rules_support_' + str(supt) + '_confidence_' + str(
    #                 confidence) + '.txt', 'r').read())
    #
    #
    # obj_id_site_stream_dict = json.loads(open('/data_txt/obj_id_site_stream_pair_dict.txt', 'r').read())

    user_sequence_dict = json.loads(
        open('/PycharmProjects/ML_Classification/data_txt/user_access_sequence_dict.txt', 'r').read())
    fp_model_site = json.loads(open(
        '/PycharmProjects/ML_Classification/data_txt/site_rules_support_' + str(
            supt) + '_confidence_' + str(
            confidence) + '.txt', 'r').read())
    fp_model_stream = json.loads(open(
        '/PycharmProjects/ML_Classification/data_txt/stream_rules_support_' + str(
            supt) + '_confidence_' + str(
            confidence) + '.txt', 'r').read())

    obj_id_site_stream_dict = json.loads(
        open('/PycharmProjects/ML_Classification/data_txt/obj_id_site_stream_pair_dict.txt', 'r').read())

    for user_id, seq_list in user_sequence_dict.items():
        if user_id not in ['0', '17', '51', '69', '2', '5', '15', '3', '7', '36',
                           '45']:  # ['20']: # ['0', '17', '51', '69']:
            # init all counter
            user_cache_hit_counter = 0
            tot_number_of_predicted_obj = 0
            site_candidate = []
            stream_candidat = []
            historical_record = []
            historical_record_site = []
            historical_record_stream = []

            print('user: ', user_id)

            for obj_id in seq_list:
                # Check if this obj_id in the previous predicted candidate list
                obj_site = obj_id_site_stream_dict[str(obj_id)][0]
                obj_stream = obj_id_site_stream_dict[str(obj_id)][1]

                if obj_id not in historical_record:
                    # Keep the record within 5 elements
                    if len(historical_record) <= 5:
                        historical_record.append(obj_id)
                        historical_record_site.append(obj_site)
                        historical_record_stream.append(obj_stream)
                    else:
                        historical_record.pop(0)
                        historical_record_site.pop(0)
                        historical_record_stream.pop(0)

                        historical_record.append(obj_id)
                        historical_record_site.append(obj_site)
                        historical_record_stream.append(obj_stream)

                if str(obj_site) in site_candidate and str(obj_stream) in stream_candidate:
                    user_cache_hit_counter += 1

                if len(site_candidate) > 5:
                    for i in range(2):
                        site_candidate.pop(0)

                if len(stream_candidate) > 5:
                    for i in range(2):
                        stream_candidate.pop(0)

                tmp_site_list = []
                tmp_stream_list = []

                # Find the candidate
                for site_ind in range(len(fp_model_site)):
                    if len(fp_model_site[site_ind][0]) <= 5:
                        if len(set(historical_record_site).intersection(fp_model_site[site_ind][0])) >= 1:
                            site_candidate.append(fp_model_site[site_ind][1])
                            tmp_site_list.append(fp_model_site[site_ind][1])
                            # tmp_counter_1 += 1
                            # if len(fp_model_site[site_ind][0]) == 5:
                            #     break

                for stream_ind in range(len(fp_model_stream)):
                    if len(fp_model_stream[stream_ind][0]) <= 5:
                        if len(set(historical_record_stream).intersection(fp_model_stream[stream_ind][0])) >= 1:
                            stream_candidate.append(fp_model_stream[stream_ind][1])
                            tmp_stream_list.append(fp_model_stream[stream_ind][1])
                            # tmp_counter_2 += 1
                            # if len(fp_model_stream[stream_ind][0]) == 5:
                            #     break

                # add current location and stream
                site_candidate.append(obj_site)
                stream_candidate.append(obj_stream)

                # print(obj_id, obj_id_site_stream_dict[str(obj_id)][0], obj_id_site_stream_dict[str(obj_id)][1])

                # update the total predicted objects

                if len(tmp_site_list) > 0:
                    tmp_site_length = len(tmp_site_list)
                else:
                    tmp_site_length = 1

                if len(tmp_stream_list) > 0:
                    tmp_stream_length = len(tmp_stream_list)
                else:
                    tmp_stream_length = 1

                tot_number_of_predicted_obj += tmp_site_length * tmp_stream_length

            statistics_dict[str(user_id)].append(user_cache_hit_counter)
            statistics_dict[str(user_id)].append(tot_number_of_predicted_obj)
            statistics_dict[str(user_id)].append(len(seq_list))
            #     print(obj_id, obj_id_site_stream_dict[str(obj_id)][0], obj_id_site_stream_dict[str(obj_id)][1])

    print(statistics_dict)
    # print('tmp_counter_1 %d, tmp_counter_2 %d' % (tmp_counter_1, tmp_counter_2) )

    # a = ['n']
    # b = [[['n'], ['m'], 0.9]]
    #
    # if list(set(a).intersection(b[0][0])) == b[0][0]:
    #     print('hi')


def count_num_of_obj_in_each_site_and_stream():
    """
    Count the number of objects in each site and stream
    This is used as tmp solution to count the number of objects that need to be moved according to prediction
    :return:
    """

    obj_id_site_stream_dict = json.loads(
        open('/PycharmProjects/ML_Classification/data_txt/obj_id_site_stream_pair_dict.txt', 'r').read())

    site_obj_dict = collections.defaultdict(list)
    stream_obj_dict = collections.defaultdict(list)

    site_obj_num = collections.defaultdict()
    stream_obj_num = collections.defaultdict()

    for obj_id, pair in obj_id_site_stream_dict.items():
        site_obj_dict[str(pair[0])].append(obj_id)
        stream_obj_dict[str(pair[1])].append(obj_id)

    for site, obj_list in site_obj_dict.items():
        site_obj_num[site] = len(obj_list)

    for stream, obj_list in stream_obj_dict.items():
        stream_obj_num[stream] = len(obj_list)

    # print(site_obj_num)
    # print(stream_obj_num)

    with open('/PycharmProjects/ML_Classification/data_txt/site_obj_num_dict.txt', 'w+') as file1:
        file1.write(json.dumps(site_obj_num))

    with open('/PycharmProjects/ML_Classification/data_txt/stream_obj_num_dict.txt', 'w+') as file2:
        file2.write(json.dump)


def generate_user_category_and_their_request_statistic_data():
    """
    It statistic how many request come from M2M user and what is the data volume
    :return:
    """
    # input_data = '/PycharmProjects/ML_Classification/data_csv/data_proc_ts_1m.csv'
    input_data = '/data_csv/data_proc_ts_sorted.csv'
    user_list = ['0', '11', '2', '15', '5', '3', '17', '7', '28', '1', '23', '8', '24', '12', '36', '31', '20', '44',
                 '45', '37', '18', '51', '19', '54', '57', '63', '65', '67', '53', '69', '66', '72']
    total_request_counter = 0
    m2m_user_request_counter = 0
    total_time_range = 0
    m2m_user_request_time_range = 0
    counter_line = 0

    read_file = open(input_data, 'r')
    line = read_file.readline()  # CSV header
    line = read_file.readline()  # First line

    while line:
        total_request_counter += 1
        # Report status
        counter_line += 1
        if counter_line % 10000 == 0:
            print('Line: ', counter_line)

        line_split = line.rstrip().split(',')
        request_start = int(line_split[3])
        request_end = int(line_split[4])
        user_id = line_split[1]
        # obj_id = line_split[2]
        total_time_range += request_end - request_start

        if user_id in user_list:
            m2m_user_request_counter += 1
            m2m_user_request_time_range += request_end - request_start

        line = read_file.readline()

    print("Total request:", total_request_counter)
    print("m2m_user_request_counter:", m2m_user_request_counter)
    print("total data volume:", total_time_range / 3600 * 10)
    print("M2M user request data volume", m2m_user_request_time_range / 3600 * 10)


def filer_out_unprefetchable_users_request():
    input_data = '/data_csv/data_proc_ts_sorted.csv'
    output_data = '/data_csv/unprefetchable_user_requests.csv'

    user_list = ['0', '11', '2', '15', '5', '3', '17', '7', '28', '1', '23', '8', '24', '12', '36', '31', '20', '44',
                 '45', '37', '18', '51', '19', '54', '57', '63', '65', '67', '53', '69', '66', '72']
    counter_line = 0

    with open(output_data, 'w') as outcsv, open(input_data, 'r') as inputcsv:

        for line in inputcsv:
            counter_line += 1
            if counter_line % 10000 == 0:
                print('Line: ', counter_line)

            line_split = line.rstrip().split(',')
            user_id = line_split[1]
            if user_id not in user_list:
                outcsv.write(line)

    print("Done")


def generate_fp_model():
    """
    This function generate the FP growth model
    :return:
    """
    import fp_growth_py3 as fpg
    import csv
    import json

    def subs(l):
        """
        Used for assRule
        """
        assert type(l) is list
        if len(l) == 1:
            return [l]
        x = subs(l[1:])
        return x + [[l[0]] + y for y in x]

    # Association rules
    def assRule(freq, min_conf=0.6):
        """
        This assRule must input a dict for itemset -> support rate
        And also can customize your minimum confidence
        """
        assert type(freq) is dict
        result = []
        for item, sup in freq.items():
            for subitem in subs(list(item)):
                sb = [x for x in item if x not in subitem]
                if sb == [] or subitem == []: continue
                if len(subitem) == 1 and (subitem[0][0] == 'in' or subitem[0][0] == 'out'):
                    continue
                conf = sup / freq[tuple(subitem)]
                if conf >= min_conf:
                    result.append({'from': subitem, 'to': sb, 'sup': sup, 'conf': conf})
        return result

    with open('//data_csv/fp_user_obj_list_0810.csv', 'r') as csv_file:
        # with open('/PycharmProjects/ML_Classification/data_csv/fp_user_obj_list_0810.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        dataset = [line for line in csv_reader]

    result = []
    res_for_rul = {}

    for supt in [60, 50, 40, 30, 20, 10]:
        result = []
        res_for_rul = {}

        fp_model = fpg.find_frequent_itemsets(dataset, minimum_support=supt, include_support=True)

        # fp_model_list = []
        for itemset, support in fp_model:  # 将generator结果存入list
            result.append((itemset, support))
            res_for_rul[tuple(itemset)] = support

        for confidence in [0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1]:  # , 0.8, 0.7, 0.6, 0.5
            rules = []
            rules = assRule(res_for_rul, confidence)
            print('confidence:', confidence)
            with open('/my_code/fp_growth/rules_support_' + str(supt) + '_confidence_' + str(
                    confidence) + '.txt', 'w+') as output_file:
                output_file.write(json.dumps(rules))


def evaluate_fp_model_performance():
    """
    This is the actual experiment code

    :return:
    """
    import fp_growth_py3 as fpg
    import csv
    import json
    import collections
    #
    SUPT_VALUE = 60
    confidence = 0.1
    historical_record = []
    user_cache_hit_counter = 0
    tot_number_of_predicted_obj = 0  # site * stream

    statistics_dict = collections.defaultdict(list)  # total hit, total predicted candidate, object sequence length

    update_flag = False

    #####
    # Result counter
    ####
    request_counter = 0
    precision_counter = 0
    recall_counter = 0

    for supt in [SUPT_VALUE]:  # [60, 50, 40, 30, 20, 10]:
        for confidence in [0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1]:

            print("Support: %d Confidence: %f" % (supt, confidence))

            user_sequence_dict = json.loads(
                open('/data_txt/user_access_sequence_dict_0810.txt', 'r').read())
            fp_model_obj = json.loads(
                open('/my_code/fp_growth/rules_support_' + str(
                    supt) + '_confidence_' + str(
                    confidence) + '.txt', 'r').read())

            # user_sequence_dict = json.loads(
            #     open('/PycharmProjects/ML_Classification/data_txt/user_access_sequence_dict_0810.txt', 'r').read())
            #
            # fp_model_obj = json.loads(open(
            #     '/PycharmProjects/ML_Classification/my_code/fp_growth/0408/rules_support_' + str(
            #         supt) + '_confidence_' + str(
            #         confidence) + '.txt', 'r').read())

            for user_id, seq_list in user_sequence_dict.items():
                # if user_id not in ['0', '17', '51', '7']: #['20']: # ['0', '17', '51', '69']:
                # init all counter
                user_cache_hit_counter = 0
                historical_record = []
                obj_candidate = set()
                request_counter = 0
                precision_counter = 0
                recall_counter = 0

                # Clean statistics_dict
                statistics_dict = collections.defaultdict(list)

                print('user: ', user_id)

                request_counter = len(seq_list)

                for obj_id in seq_list:

                    if obj_id not in historical_record:
                        update_flag = True
                        # Keep the record within 5 elements
                        if len(historical_record) <= 5:
                            historical_record.append(obj_id)
                        else:
                            historical_record.pop(0)
                            historical_record.append(obj_id)

                    if str(obj_id) in obj_candidate:
                        user_cache_hit_counter += 1
                        precision_counter += 1 / len(obj_candidate)
                        recall_counter += 1

                    tmp_obj_list = []
                    tmp_site_obj_num = 0
                    tmp_stream_obj_num = 0

                    # Find the candidate
                    if update_flag:
                        update_flag = False
                        obj_candidate.clear()
                        for obj_indx in range(len(fp_model_obj)):
                            if len(fp_model_obj[obj_indx]['from']) <= 5:
                                if len(set(historical_record).intersection(
                                        fp_model_obj[obj_indx]['from'])) >= 1:  # len(historical_record)*0.5:
                                    obj_candidate.update(fp_model_obj[obj_indx]['to'])
                                    # Add object itself
                                    obj_candidate.add(obj_id)

                statistics_dict[str(user_id)].append(request_counter)
                statistics_dict[str(user_id)].append(user_cache_hit_counter)
                statistics_dict[str(user_id)].append(precision_counter / request_counter)
                statistics_dict[str(user_id)].append(recall_counter / request_counter)

            #
            # with open('/PycharmProjects/ML_Classification/my_code/fp_growth/0408/result_support_' + str(
            #     supt) + '_confidence_' + str(confidence) + '.csv', 'w+') as output_file:
            #     out_writer = csv.writer(output_file, delimiter=',')
            #     for key in statistics_dict:
            #         out_writer.writerow([key] + statistics_dict[key])

            with open('/my_code/fp_growth/fp_result/result_support_' + str(
                    supt) + '_confidence_' + str(confidence) + '.csv', 'w+') as output_file:
                out_writer = csv.writer(output_file, delimiter=',')
                for key in statistics_dict:
                    out_writer.writerow([key] + statistics_dict[key])


def ooi_user_classification():
    import csv, collections, statistics, json

    # DIR = '/PycharmProjects/ML_Classification/data_csv/'
    # INPUT = 'data_sample_test.csv'
    DIR = '/data_csv/'
    INPUT = 'data_proc_ts_sorted.csv'

    user_obj_specs_dict = collections.defaultdict(lambda: collections.defaultdict(
        lambda: [0, 0, 0, 0, 0, 0, 0, 0, 0]))  # {user_id: {obj_id: [tot request, last ts, tot data volume, \
    # last end time range, faulty counter, \
    # legitimate data volume, faulty data volume, avg interval, median]}}
    user_obj_interval_dict = collections.defaultdict(lambda: collections.defaultdict(list))
    # user_obj_avg_interval_dict = collections.defaultdict(lambda: collections.defaultdict())

    interactive_user_list = []
    program_user_list = []

    data_density = 10  # KB/h

    counter = 0

    with open(DIR + INPUT, 'r', encoding='utf-8-sig') as inputfile:
        # with open(DIR + OUTPUT, 'w+') as outputfile:
        csv_reader = csv.reader(inputfile)
        next(csv_reader, None)
        # csv_writer = csv.writer(outputfile)

        for line in csv_reader:
            cur_ts = int(line[0])
            user_id = line[1]
            obj_id = line[2]
            tr_start = int(line[3])
            tr_end = int(line[4])

            if counter % 10000 == 0:
                print('Counter: ', counter)
            counter += 1

            if obj_id in user_obj_specs_dict[user_id]:
                # update total request
                user_obj_specs_dict[user_id][obj_id][0] += 1
                # get last ts
                last_ts = user_obj_specs_dict[user_id][obj_id][1]
                # Add the interval
                user_obj_interval_dict[user_id][obj_id].append(abs(cur_ts - int(last_ts)))
                # update last_ts
                user_obj_specs_dict[user_id][obj_id][1] = cur_ts
                # update data volume
                user_obj_specs_dict[user_id][obj_id][2] += abs(tr_end - tr_start) / 3600 / 1024 * data_density  # MB
                # last end time range
                last_tr_end = int(user_obj_specs_dict[user_id][obj_id][3])
                # check if it is faulty request
                if last_tr_end - tr_start >= 0.1 * (tr_end - tr_start):  # time range overlap larger than 1 min
                    # Increase faulty request counter
                    user_obj_specs_dict[user_id][obj_id][4] += 1
                    # Add to faulty data volume
                    user_obj_specs_dict[user_id][obj_id][6] += abs(tr_end - tr_start) / 3600 / 1024 * data_density  # MB
                else:
                    # Add to legitimate data volume
                    user_obj_specs_dict[user_id][obj_id][5] += abs(tr_end - tr_start) / 3600 / 1024 * data_density  # MB

                # update last end time range
                user_obj_specs_dict[user_id][obj_id][3] = tr_end

            else:
                user_obj_specs_dict[user_id][obj_id] = [1, cur_ts, abs(tr_end - tr_start) / 3600 / 1024 * data_density,
                                                        tr_end, 0, abs(tr_end - tr_start) / 3600 / 1024 * data_density,
                                                        0, 0, 0]

        print('Dump the dict')
        # Dump dict
        with open(DIR + 'ooi_user_obj_specs_dict.txt', 'w+') as outputfile1:
            outputfile1.write(json.dumps(user_obj_specs_dict))

        with open(DIR + 'ooi_user_obj_interval_dict.txt', 'w+') as outputfile2:
            outputfile2.write(json.dumps(user_obj_interval_dict))
        ################
        # Process result
        ################

    ##########
    # Read the data back
    ##########

    # import csv, collections, statistics, json
    #
    # # DIR = '/PycharmProjects/ML_Classification/data_csv/'
    # # INPUT = 'data_sample_test.csv'
    # DIR = '/data_csv/'
    # INPUT = 'data_proc_ts_sorted.csv'
    #
    # user_obj_specs_dict = collections.defaultdict(lambda: collections.defaultdict(lambda: [0, 0, 0, 0,
    #                                                                                        0]))
    # {user_id: {obj_id: [tot request, last ts, data volume, last end time range, faulty counter]}}
    # user_obj_interval_dict = collections.defaultdict(lambda: collections.defaultdict(list))
    # # user_obj_avg_interval_dict = collections.defaultdict(lambda: collections.defaultdict())
    #
    # interactive_user_list = []
    # program_user_list = []
    #
    # data_density = 10  # MB/h
    #
    # counter = 0
    #
    #
    # with open(DIR + 'ooi_user_obj_specs_dict.txt', 'r') as inputfile1:
    #     user_obj_specs_dict = json.loads(inputfile1.read())
    #
    # # with open(DIR + 'ooi_user_obj_interval_dict.txt', 'r') as inputfile2:
    # #     user_obj_interval_dict = json.loads(inputfile2.read())
    #
    #
    # with open(DIR+'user_obj_specs_dict_readable.csv', 'w+') as output_csv:
    #     csv_writer = csv.writer(output_csv)
    #
    #     for user_id, obj_dict in user_obj_specs_dict.items():
    #         for obj_id, obj_list in obj_dict.items():
    #             csv_writer.writerow([user_id+'#'+obj_id, obj_list])

    #####Read end##########

    print("Start Distinguish user classes")

    # Distinguish user classes
    for user_id, obj_dict in user_obj_specs_dict.items():
        program_flag = False
        for obj_id, obj_list in obj_dict.items():
            if obj_list[0] >= 28:  # at least daily request
                program_flag = True

        if program_flag:
            program_user_list.append(user_id)
        else:
            interactive_user_list.append(user_id)

    print("Start Get data volume")

    # Get data volume
    interactive_user_data = 0
    program_user_data = 0

    for user_id, obj_dict in user_obj_specs_dict.items():
        if user_id in interactive_user_list:
            for obj_id, obj_list in obj_dict.items():
                interactive_user_data += obj_list[2]

                # Update the average interval info
                try:
                    avg_interval = statistics.mean(user_obj_interval_dict[user_id][obj_id])
                    median_interval = statistics.median(user_obj_interval_dict[user_id][obj_id])
                except:
                    avg_interval = -1
                    median_interval = -1

                user_obj_specs_dict[user_id][obj_id][7] = avg_interval
                user_obj_specs_dict[user_id][obj_id][8] = median_interval


        elif user_id in program_user_list:
            for obj_id, obj_list in obj_dict.items():
                program_user_data += obj_list[2]

                # Update the average interval info
                try:
                    avg_interval = statistics.mean(user_obj_interval_dict[user_id][obj_id])
                except:
                    avg_interval = -1

                user_obj_specs_dict[user_id][obj_id][-1] = avg_interval
        else:
            print("ERROR: user_id belong to neither interactive user nor program user")

    print("Start Get data volume for these three type of users separately")

    # Get data volume for these three type of users separately
    program_realtime_data_volume = 0
    program_normal_data_volume = 0
    program_faulty_data_volume = 0
    interactive_faulty_data_volume = 0

    for user_id, obj_dict in user_obj_specs_dict.items():
        if user_id not in program_user_list:
            for obj_id, obj_list in obj_dict.items():
                interactive_faulty_data_volume += obj_list[6]

        else:
            for obj_id, obj_list in obj_dict.items():
                program_faulty_data_volume += obj_list[6]

                avg_interval = obj_list[-1]

                if 0 < avg_interval <= 60:  # avg interval less than 1 mins, realtime program
                    program_realtime_data_volume += obj_list[5]
                else:
                    program_normal_data_volume += obj_list[5]

    # Dump the dict
    with open(DIR + 'user_obj_specs_dict_readable.csv', 'w+') as output_csv:
        csv_writer = csv.writer(output_csv, delimiter=',')

        for user_id, obj_dict in user_obj_specs_dict.items():
            for obj_id, obj_list in obj_dict.items():
                # csv_writer.writerow(user_id+'#'+obj_id)
                line = [user_id + '#' + obj_id] + obj_list
                csv_writer.writerow(line)

        csv_writer.writerow(('interactive_user', interactive_user_list))
        csv_writer.writerow(('program_user', program_user_list))

    # Print result
    print('Total interactive user: %s' % len(interactive_user_list))
    print('Total program user: %s' % len(program_user_list))
    print('Total interactive user data volume: %s' % interactive_user_data)
    print('Total program user data volume: %s' % program_user_data)
    print('Total realtime program data volume: %s' % program_realtime_data_volume)
    print('Total normal program data volume: %s' % program_normal_data_volume)
    print('Total faulty program data volume: %s' % program_faulty_data_volume)
    print('Total faulty interactive data volume: %s' % interactive_faulty_data_volume)

    # Save result
    with open(DIR + 'ooi_user_classification_result.txt', 'w+') as outputfile3:
        outputfile3.write('Total interactive user: %s\n' % len(interactive_user_list))
        outputfile3.write('Total program user: %s\n' % len(program_user_list))

        outputfile3.write('Total interactive user data volume: %s\n' % interactive_user_data)
        outputfile3.write('Total program user data volume: %s\n' % program_user_data)

        outputfile3.write('Total realtime program data volume: %s\n' % program_realtime_data_volume)
        outputfile3.write('Total normal program data volume: %s\n' % program_normal_data_volume)
        outputfile3.write('Total faulty program data volume: %s\n' % program_faulty_data_volume)
        outputfile3.write('Total faulty interactive data volume: %s' % interactive_faulty_data_volume)


def find_ooi_faulty_fresh_and_cacheable_data_volume():
    """
    For faulty users, including program and interactive users, this one find the fresh data volume and cacheable data volume
    Fresh means the first time retrieve data, the cacheable data is the part that already in the cache
    :return:
    """
    # def ooi_user_classification():
    import csv, collections, json

    # DIR = '/PycharmProjects/ML_Classification/data_csv/'
    # INPUT = 'data_sample_test.csv'
    DIR = '/data_csv/'
    INPUT = 'data_proc_ts_sorted.csv'

    user_obj_specs_dict = collections.defaultdict(lambda: collections.defaultdict(
        lambda: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]))  # {user_id: {obj_id: [tot request, last ts, tot data volume, \
    # last end time range, faulty counter, \
    # legitimate data volume, faulty data volume, avg interval, median, faulty_fresh, faulty_cacheable]}}
    # user_obj_interval_dict = collections.defaultdict(lambda: collections.defaultdict(list))
    # user_obj_avg_interval_dict = collections.defaultdict(lambda: collections.defaultdict())

    interactive_user_list = []
    program_user_list = []

    data_density = 10  # KB/h

    counter = 0

    with open(DIR + INPUT, 'r', encoding='utf-8-sig') as inputfile:
        # with open(DIR + OUTPUT, 'w+') as outputfile:
        csv_reader = csv.reader(inputfile)
        next(csv_reader, None)
        # csv_writer = csv.writer(outputfile)

        for line in csv_reader:
            cur_ts = int(line[0])
            user_id = line[1]
            obj_id = line[2]
            tr_start = int(line[3])
            tr_end = int(line[4])

            if counter % 10000 == 0:
                print('Counter: ', counter)
            counter += 1

            if obj_id in user_obj_specs_dict[user_id]:
                # update total request
                user_obj_specs_dict[user_id][obj_id][0] += 1
                # get last ts
                # last_ts = user_obj_specs_dict[user_id][obj_id][1]
                # # Add the interval
                # user_obj_interval_dict[user_id][obj_id].append(abs(cur_ts - int(last_ts)))
                # update last_ts
                # user_obj_specs_dict[user_id][obj_id][1] = cur_ts
                # update data volume
                # user_obj_specs_dict[user_id][obj_id][2] += abs(tr_end - tr_start) / 3600 / 1024 * data_density  # MB
                # last end time range
                last_tr_end = int(user_obj_specs_dict[user_id][obj_id][3])
                # check if it is faulty request
                if last_tr_end - tr_start >= 0.1 * (tr_end - tr_start):  # time range overlap larger than 10%
                    # # Increase faulty request counter
                    # user_obj_specs_dict[user_id][obj_id][4] += 1
                    # # Add to faulty data volume
                    user_obj_specs_dict[user_id][obj_id][6] += abs(tr_end - tr_start) / 3600 / 1024 * data_density  # MB
                    # Faulty fresh
                    if last_tr_end > tr_end:  # last time range was too large, all new request is cacheable
                        user_obj_specs_dict[user_id][obj_id][10] += abs(tr_end - tr_start) / 3600 / 1024 * data_density
                    else:
                        user_obj_specs_dict[user_id][obj_id][9] += abs(
                            tr_end - last_tr_end) / 3600 / 1024 * data_density  # MB
                        user_obj_specs_dict[user_id][obj_id][10] += abs(
                            last_tr_end - tr_start) / 3600 / 1024 * data_density  # MB

                # else:
                #     # Add to legitimate data volume
                #     user_obj_specs_dict[user_id][obj_id][5] += abs(tr_end - tr_start) / 3600 / 1024 * data_density  # MB

                # update last end time range
                user_obj_specs_dict[user_id][obj_id][3] = tr_end

            else:
                user_obj_specs_dict[user_id][obj_id] = [1, cur_ts, abs(tr_end - tr_start) / 3600 / 1024 * data_density,
                                                        tr_end, 0, abs(tr_end - tr_start) / 3600 / 1024 * data_density,
                                                        0, 0, 0, 0, 0]

        print('Dump the dict')

        with open(DIR + 'ooi_user_obj_specs_dict_with_faulty_statistic.txt', 'w+') as outputfile1:
            outputfile1.write(json.dumps(user_obj_specs_dict))

    ##############
    # Read from file
    #############

    # with open(DIR + 'ooi_user_obj_specs_dict_with_faulty_statistic.txt', 'r') as input_file:
    #     user_obj_specs_dict = json.loads(input_file.read())

    #####Read end##########

    print("Start Distinguish user classes")

    # Distinguish user classes
    for user_id, obj_dict in user_obj_specs_dict.items():
        program_flag = False
        for obj_id, obj_list in obj_dict.items():
            if obj_list[0] >= 28:  # at least daily request
                program_flag = True

        if program_flag:
            program_user_list.append(user_id)
        else:
            interactive_user_list.append(user_id)

    print("Start Get data volume")

    inter_fresh_data = 0
    inter_cachable_data = 0
    program_fresh_data = 0
    program_cachable_data = 0
    fresh_data = 0
    cachable_data = 0
    tot_fualty_data = 0

    counter = 0

    for user_id, obj_dict in user_obj_specs_dict.items():

        if counter % 1 == 0:
            print('#User Counter: ', counter)
        counter += 1

        for obj_id, obj_list in obj_dict.items():
            fresh_data = int(obj_list[9])
            cachable_data = int(obj_list[10])
            tot_fualty_data += int(obj_list[6])

            # if total_request_counter > 2:  # Faulty request
            if user_id in interactive_user_list:
                inter_fresh_data += fresh_data
                inter_cachable_data += cachable_data
            else:
                program_fresh_data += fresh_data
                program_cachable_data += cachable_data

    print('Faulty program fresh data volume: %s\n' % program_fresh_data)
    print('Faulty program cachable data volume: %s\n' % program_cachable_data)
    print('Faulty interactive fresh data volume: %s\n' % inter_fresh_data)
    print('Faulty interactive cachable data volume: %s\n' % inter_cachable_data)
    print('Faulty total data volume: %s\n' % tot_fualty_data)


def extract_duplicated_request():
    """
    Extract the duplicated request (consecutive) and output them into another file
    :return:
    """

    DATA_DIR = '/data_csv/'
    FILE_1 = 'data_proc.csv'
    FILE_2 = 'ooi_duplicated_request.csv'

    with open(DATA_DIR + FILE_1, 'r') as in_file:
        with open(DATA_DIR + FILE_2, 'w+') as out_file:
            seen = set()

            for line in in_file:
                line_split = line.split(',')
                key = str(line_split[0] + '#' + line_split[2] + '#' + line_split[3] + '#' + line_split[4] + '#' \
                          + line_split[5] + '#' + line_split[6] + '#' + line_split[7])

                if key in seen:
                    out_file.write(line)

                seen.add(key)


def extract_realtime_and_non_realtime_request_out():
    """
    It define the creteria for realtime and non-realtime request
    It extract the non-realtime requests into a separated CSV file
    The files will be used to feed into the prediction model
    :return:
    """
    import csv, collections
    import os
    import glob

    # INPUT_DIR = "/PycharmProjects/ML_Classification/data_csv/"
    INPUT_DIR = "/data_csv/"
    # INPUT_DIR = "/unavco/data_csv/"
    PREFETCHABLE_DIR = "/data_csv/ooi_pref/"
    # PREFETCHABLE_DIR = "/unavco/data_csv/unavco_pref/"
    STREAMABLE_DIR = "/data_csv/ooi_stream/"
    # STREAMABLE_DIR = "/unavco/data_csv/ooi_stream/"
    # INPUT_FILE = "data_proc_ts_1m.csv"
    INPUT_FILE = "data_proc_ts_sorted.csv"
    # INPUT_FILE = "out_2018.json_new_obj_id.csv"

    # TO record the normal requests
    normal_req_file = "ooi_normal_reqs.csv"

    # Remove all previous data in the prefetchable folder
    files = glob.glob(PREFETCHABLE_DIR + "*")
    for f in files:
        os.remove(f)

    # Remove all previous data in the streamable folder
    files = glob.glob(STREAMABLE_DIR + "*")
    for f in files:
        os.remove(f)

    # Implement online user request classification
    # Given the detection window size as 2 weeks
    # Define the detection window data structure

    ########################
    # user_obj_intv_dict
    # It records the user historical requests within the given window size
    # The access interval is the key data to classify user requests
    # {user_id: {obj_id: [[TS, interval], [TS, interval], ...]}}
    ########################
    user_obj_intv_dict = collections.defaultdict(lambda: collections.defaultdict(lambda: collections.deque()))

    ########################
    # user_obj_avg_intv_dict
    # It records the user average access interval.
    # This is used for classifying realtime users
    # {user_id: {obj_id: avg_intv}}
    ########################
    user_obj_avg_intv_dict = collections.defaultdict(lambda: collections.defaultdict(lambda: 0))

    # Detection window size
    window_size_day = 7
    window_size_sec = window_size_day * 24 * 3600  # 2 weeks in seconds

    # Realtime user#obj dict
    realtime_user_obj = collections.defaultdict()

    # Non-realtime user#obj dict
    non_realtime_user_obj = collections.defaultdict()

    # Read the input file
    # Currently, targeting to the OOI log
    with open(INPUT_DIR + INPUT_FILE, 'r') as input_file:
        with open(INPUT_DIR + normal_req_file, 'a+') as normal_output:
            input_csv = csv.reader(input_file)
            output_csv = csv.writer(normal_output)
            next(input_csv, None)  # skip the header

            counter = 0

            for line in input_csv:  # access_time,userID,sensor_data,begin_time,end_time
                current_ts = int(line[0])
                user_id = str(line[1])
                obj_id = str(line[2])

                if counter % 1000000 == 0:
                    print('Counter: ', counter)
                counter += 1

                if user_id + "#" + obj_id in realtime_user_obj or user_id + "#" + obj_id in non_realtime_user_obj:
                    # Write to the CSV file
                    # Output the prefetchable request.
                    if user_id + "#" + obj_id in non_realtime_user_obj:
                        # Append the line into corresponding CSV file
                        with open(PREFETCHABLE_DIR + user_id + "#" + obj_id + ".csv", 'a+') as output_file:
                            csv_writer = csv.writer(output_file)
                            csv_writer.writerow(line)

                    elif user_id + "#" + obj_id in realtime_user_obj:
                        # Append the line into corresponding CSV file
                        with open(STREAMABLE_DIR + user_id + "#" + obj_id + ".csv", 'a+') as output_file:
                            csv_writer = csv.writer(output_file)
                            csv_writer.writerow(line)

                    continue

                # Record this request as the normal request
                output_csv.writerow(line)

                # Insert data point
                if user_id in user_obj_intv_dict and obj_id in user_obj_intv_dict[user_id]:
                    last_ts = user_obj_intv_dict[user_id][obj_id][-1][0]  # last elem's TS
                    time_interval = current_ts - last_ts
                    elem = [current_ts, time_interval]
                    user_obj_intv_dict[user_id][obj_id].append(elem)

                else:
                    elem = [current_ts, 0]
                    user_obj_intv_dict[user_id][obj_id].append(elem)

                # Evict data point out of the detection window
                # Only examine current user and current data object
                curr_window_size = user_obj_intv_dict[user_id][obj_id][-1][0] - user_obj_intv_dict[user_id][obj_id][0][
                    0]  # last ts - first ts

                if curr_window_size > window_size_sec:  # Then, remove the left most element
                    #######
                    # Detect non-realtime users
                    ######
                    if user_obj_avg_intv_dict[user_id][obj_id] > 60 and len(user_obj_intv_dict[user_id][obj_id]) >= 14:
                        non_realtime_user_obj[user_id + "#" + obj_id] = 1

                        # Delete user_obj_intv_dict[user_id][obj_id]
                        del user_obj_intv_dict[user_id][obj_id]

                    else:
                        # pop the left element
                        user_obj_intv_dict[user_id][obj_id].popleft()

                # Calculate average access interval
                # Though this loop is costly, but the data points are restricted within the window_size_day
                # This cost is acceptable
                tmp_sum = 0
                for elem_list in user_obj_intv_dict[user_id][obj_id]:
                    tmp_sum += elem_list[1]

                if len(user_obj_intv_dict[user_id][obj_id]) > 0:
                    avg_intv = tmp_sum / len(user_obj_intv_dict[user_id][obj_id])

                    user_obj_avg_intv_dict[user_id][obj_id] = avg_intv  # Update the average value

                ############
                # Detect realtime user
                # Average interval less than 60 sec and there are more than 100 requests within the detection window
                ############
                if user_obj_avg_intv_dict[user_id][obj_id] <= 60 and len(user_obj_intv_dict[user_id][obj_id]) >= 100:
                    realtime_user_obj[user_id + "#" + obj_id] = 1
                    # Delete this item
                    del user_obj_intv_dict[user_id][obj_id]

                ###########
                # Detect non-realtime user
                # 1) Avg interval larger than 60 sec and there are more than 168 requests
                #    The 168 = 24 reqs * 7 days. This is based on hourly-based pattern
                # 2) Within the entire detection window. The Avg interval larger than 60 sec
                #    and there are more than 'window_size_day' requests.
                #    This is based on daily request pattern
                ###########
                if user_obj_avg_intv_dict[user_id][obj_id] > 60 and len(
                        user_obj_intv_dict[user_id][obj_id]) >= 24 * window_size_day / 2:
                    non_realtime_user_obj[user_id + "#" + obj_id] = 1
                    # Delete this item
                    del user_obj_intv_dict[user_id][obj_id]


def extract_normal_request_out():
    """
    Use the same filter criterias as the above function. But output the normal requests
    :return:
    """
    import csv, collections
    import os
    import glob
    from operator import itemgetter

    # INPUT_DIR = "/PycharmProjects/ML_Classification/data_csv/ooi_simulation/"
    INPUT_DIR = "/data_csv/sample_files/"
    # INPUT_DIR = "/unavco/data_csv/"
    # PREFETCHABLE_DIR = "/data_csv/tmp_simulation/ooi_p/"
    # PREFETCHABLE_DIR = "/PycharmProjects/ML_Classification/data_csv/ooi_simulation/ooi_p/"
    PREFETCHABLE_DIR = "/data_csv/sample_files/ooi_p/"
    STREAMABLE_DIR = "/data_csv/sample_files/ooi_s/"
    # STREAMABLE_DIR = "/PycharmProjects/ML_Classification/data_csv/ooi_simulation/ooi_s/"
    # STREAMABLE_DIR = "/unavco/data_csv/ooi_stream/"

    ## interactive user uses API to download data, similar to streaming but in a short time
    INT_STREAMABLE_DIR = "/data_csv/sample_files/ooi_is/"
    # INT_STREAMABLE_DIR = "/PycharmProjects/ML_Classification/data_csv/ooi_simulation//ooi_is/"

    # INPUT_FILE = "/PycharmProjects/ML_Classification/data_csv/ooi_simulation/data_proc_ts_1m.csv"
    INPUT_FILE = "/data_csv/data_proc_ts.csv"
    # INPUT_FILE = "out_2018.json_new_obj_id.csv"

    # TO record the normal requests
    normal_req_file = "ooi_normal_reqs.csv"

    # Remove all previous data in the prefetchable folder
    files = glob.glob(PREFETCHABLE_DIR + "*")
    for f in files:
        os.remove(f)

    # Remove all previous data in the streamable folder
    files = glob.glob(STREAMABLE_DIR + "*")
    for f in files:
        os.remove(f)

    DEBUG_FLAG = False

    # Implement online user request classification
    # Given the detection window size as 2 weeks
    # Define the detection window data structure

    ########################
    # user_obj_intv_dict
    # It records the user historical requests within the given window size
    # The access interval is the key data to classify user requests
    # {user_id: {obj_id: [[TS, interval], [TS, interval], ...]}}
    ########################
    user_obj_intv_dict = collections.defaultdict(lambda: collections.defaultdict(lambda: collections.deque()))

    ########################
    # user_obj_avg_intv_dict
    # It records the user average access interval.
    # This is used for classifying realtime users
    # {user_id: {obj_id: avg_intv}}
    ########################
    user_obj_avg_intv_dict = collections.defaultdict(lambda: collections.defaultdict(lambda: 0))

    ########################
    # Store the qualified request into dict rather than output to files
    # This is useful in speeding up and selecting interactive user streaming requests
    # {uesr#obj: [[req], [req]]}

    prefetchable_request_dict = collections.defaultdict(lambda: collections.deque())
    streamable_request_dict = collections.defaultdict(lambda: collections.deque())

    # Detection window size
    window_size_day = 7
    window_size_sec = window_size_day * 24 * 3600  # 2 weeks in seconds

    # Streaming threshold, only the time length larger than this threshold can be treated as streaming request
    # Otherwise, it is interactive user API request
    streaming_threshold = 3600 * 24 * 1  # One day in seconds

    # Realtime user#obj dict
    realtime_user_obj = collections.defaultdict()

    # Non-realtime user#obj dict
    non_realtime_user_obj = collections.defaultdict()

    # Count the user_ID#obj_ID
    debug_counter = collections.defaultdict(lambda: 0)
    debug_prefetchable_obj_list = INPUT_DIR + "debug_prefetchable_obj_list.csv"
    debug_streamable_obj_list = INPUT_DIR + "debug_streamable_obj_list.csv"

    # Read the input file
    # Currently, targeting to the OOI log
    with open(INPUT_FILE, 'r') as input_file:
        with open(INPUT_DIR + normal_req_file, 'a+') as normal_output:
            input_csv = csv.reader(input_file)
            output_csv = csv.writer(normal_output)
            next(input_csv, None)  # skip the header

            counter = 0

            for line in input_csv:  # access_time,userID,sensor_data,begin_time,end_time
                current_ts = int(line[0])
                user_id = str(line[1])
                obj_id = str(line[2])

                if DEBUG_FLAG:  # Count the reqests for each user and object
                    tmp_name = user_id + "#" + obj_id
                    debug_counter[tmp_name] += 1

                if counter % 100000 == 0:
                    print('Counter: ', counter)
                counter += 1

                if user_id + "#" + obj_id in realtime_user_obj or user_id + "#" + obj_id in non_realtime_user_obj:
                    # Write to the CSV file
                    # Output the prefetchable request.
                    if user_id + "#" + obj_id in non_realtime_user_obj:
                        prefetchable_request_dict[user_id + "#" + obj_id].append(line)
                        # Append the line into corresponding CSV file
                        # with open(PREFETCHABLE_DIR + user_id + "#" + obj_id + ".csv", 'a+') as output_file_p:
                        #     csv_writer = csv.writer(output_file_p)
                        #     csv_writer.writerow(line)

                    elif user_id + "#" + obj_id in realtime_user_obj:
                        # Append the line into corresponding CSV file
                        streamable_request_dict[user_id + "#" + obj_id].append(line)
                        # with open(STREAMABLE_DIR + user_id + "#" + obj_id + ".csv", 'a+') as output_file_s:
                        #     csv_writer = csv.writer(output_file_s)
                        #     csv_writer.writerow(line)

                    continue

                # Record this request as the normal request
                output_csv.writerow(line)

                # Insert data point
                if user_id in user_obj_intv_dict and obj_id in user_obj_intv_dict[user_id]:
                    last_ts = user_obj_intv_dict[user_id][obj_id][-1][0]  # last elem's TS
                    time_interval = current_ts - last_ts
                    elem = [current_ts, time_interval]
                    user_obj_intv_dict[user_id][obj_id].append(elem)

                else:
                    elem = [current_ts, 0]
                    user_obj_intv_dict[user_id][obj_id].append(elem)

                # Evict data point out of the detection window
                # Only examine current user and current data object
                curr_window_size = user_obj_intv_dict[user_id][obj_id][-1][0] - user_obj_intv_dict[user_id][obj_id][0][
                    0]  # last ts - first ts

                if curr_window_size > window_size_sec:  # Then, remove the left most element
                    #######
                    # Detect non-realtime users
                    ######
                    if user_obj_avg_intv_dict[user_id][obj_id] > 60 and len(user_obj_intv_dict[user_id][obj_id]) >= 14:
                        non_realtime_user_obj[user_id + "#" + obj_id] = 1

                        # Delete user_obj_intv_dict[user_id][obj_id]
                        del user_obj_intv_dict[user_id][obj_id]
                        continue

                    else:
                        # pop the left element
                        user_obj_intv_dict[user_id][obj_id].popleft()

                # Calculate average access interval
                # Though this loop is costly, but the data points are restricted within the window_size_day
                # This cost is acceptable
                tmp_sum = 0
                for elem_list in user_obj_intv_dict[user_id][obj_id]:
                    tmp_sum += elem_list[1]

                if len(user_obj_intv_dict[user_id][obj_id]) > 0:
                    avg_intv = tmp_sum / len(user_obj_intv_dict[user_id][obj_id])

                    user_obj_avg_intv_dict[user_id][obj_id] = avg_intv  # Update the average value

                ############
                # Detect realtime user
                # Average interval less than 60 sec and there are more than 100 requests within the detection window
                ############
                if user_obj_avg_intv_dict[user_id][obj_id] <= 60 and len(user_obj_intv_dict[user_id][obj_id]) >= 100:
                    realtime_user_obj[user_id + "#" + obj_id] = 1
                    # Delete this item
                    del user_obj_intv_dict[user_id][obj_id]
                    continue

                ###########
                # Detect non-realtime user
                # 1) Avg interval larger than 60 sec and there are more than 168 requests
                #    The 168 = 24 reqs * 7 days. This is based on hourly-based pattern
                # 2) Within the entire detection window. The Avg interval larger than 60 sec
                #    and there are more than 'window_size_day' requests.
                #    This is based on daily request pattern
                ###########
                if user_obj_avg_intv_dict[user_id][obj_id] > 60 and len(
                        user_obj_intv_dict[user_id][obj_id]) >= 24 * window_size_day / 2:
                    non_realtime_user_obj[user_id + "#" + obj_id] = 1
                    # Delete this item
                    del user_obj_intv_dict[user_id][obj_id]
                    continue

    print("Start outputing files")

    # Output the request to files
    # Prefetchable requests
    # TODO： 需要对TS排序
    for user_obj, req_list in prefetchable_request_dict.items():
        # Sort req_list according to the TS
        req_list = sorted(req_list, key=itemgetter(0))
        with open(PREFETCHABLE_DIR + user_obj + ".csv", "w+") as output_file_p:
            pref_writer = csv.writer(output_file_p)
            pref_writer.writerows(req_list)

    # Streaming
    for user_obj, req_list in streamable_request_dict.items():
        # Check if it is streaming or interactive API request
        # TODO: 还需要继续完善
        # Sort req_list according to the TS
        req_list = sorted(req_list, key=itemgetter(0))

        time_lenght = int(req_list[-1][0]) - int(req_list[0][0])  # last request TS - first request TS
        if time_lenght >= streaming_threshold and len(
                req_list) >= 1440 * window_size_day / 2:  # Daily streaming at every minutes
            with open(STREAMABLE_DIR + user_obj + ".csv", "w+") as output_file_s:
                stream_writer = csv.writer(output_file_s)
                stream_writer.writerows(req_list)

        else:  # interactive users' API requests
            with open(INT_STREAMABLE_DIR + user_obj + ".csv", "w+") as output_file_is:
                int_stream_writer = csv.writer(output_file_is)
                int_stream_writer.writerows(req_list)

    # Debug, count the obj call time
    if DEBUG_FLAG:
        # Output each streamable and prefetchabel user and its requested object
        # count prefetchable objects
        with open(debug_prefetchable_obj_list, "w+") as debug_out_p:
            debug_writer_p = csv.writer(debug_out_p)
            for obj_name, val in non_realtime_user_obj.items():
                cnt_p = debug_counter[obj_name]
                print("prefetchable:", obj_name, cnt_p)
                debug_writer_p.writerow([obj_name, cnt_p])

        # count streamable objects
        with open(debug_streamable_obj_list, "w+") as debug_out_s:
            debug_writer_s = csv.writer(debug_out_s)
            for obj_name, val in realtime_user_obj.items():
                cnt_s = debug_counter[obj_name]
                print("streamable:", obj_name, cnt_s)
                debug_writer_s.writerow([obj_name, cnt_s])


def add_request_type_to_log():
    """
    Adding the request type "n", "s", "p" to the existing log file
    :return:
    """
    import csv, os

    INPUT_DIR = "/PycharmProjects/ML_Classification/data_csv/OOI_sample/"
    OUTPUT_DIR = "/PycharmProjects/ML_Classification/data_csv/OOI_sample/"

    # INPUT_FILE = "ooi_normal_1k.csv"
    INPUT_FILE = "ooi_stream_1k.csv"
    # INPUT_FILE = "ooi_pref_1k.csv"

    REQ_TYPE = "p"

    with open(INPUT_DIR + INPUT_FILE, 'r') as input_file:
        with open(OUTPUT_DIR + "TMP_" + INPUT_FILE, "w+") as output_file:
            csv_reader = csv.reader(input_file)
            csv_writer = csv.writer(output_file)

            for line in csv_reader:
                csv_writer.writerow(line + [REQ_TYPE])

    # Replace the file
    os.remove(INPUT_DIR + INPUT_FILE)
    os.rename(INPUT_DIR + "TMP_" + INPUT_FILE, INPUT_DIR + INPUT_FILE)


def combine_all_file_into_one_log_file():
    """
    There are multiple files containing streaming, prefetching and normal request.
    This function combines all these files into one CSV file
    :return:
    """
    import csv

    INPUT_DIR = "/PycharmProjects/ML_Classification/data_csv/OOI_sample/"

    FILE_LIST = "file_list.txt"

    COMBINED_FILE = "ooi_request_combined.csv"

    with open(INPUT_DIR + COMBINED_FILE, 'w+') as output_file:
        csv_writer = csv.writer(output_file)

        with open(INPUT_DIR + FILE_LIST, "r") as file_list:
            for file_name in file_list:
                with open(INPUT_DIR + file_name.rstrip(), "r") as input_file:
                    csv_reader = csv.reader(input_file)

                    # Merge all individual files into the combined file
                    for line in csv_reader:
                        csv_writer.writerow(line)


def sort_the_combined_file():
    """
    Sort the combined file according to the TS in ascending order
    :return:
    """
    import csv
    import operator

    INPUT_DIR = "/PycharmProjects/ML_Classification/data_csv/OOI_sample/"

    COMBINED_FILE = "ooi_request_combined.csv"
    SORTED_FILE = "ooi_request_sorted.csv"

    with open(INPUT_DIR + COMBINED_FILE, 'r') as input_file:
        with open(INPUT_DIR + SORTED_FILE, "w+") as output_file:
            csv_reader = csv.reader(input_file)
            csv_writer = csv.writer(output_file)
            sortedlist = sorted(csv_reader, key=operator.itemgetter(0))

            for line in sortedlist:
                csv_writer.writerow(line)


def remove_prefix_in_the_filename():
    """
    Remove the prefix from the file name, such as "TMP_" or "OUT_"
    :return:
    """
    import glob, os

    DIR = "/PycharmProjects/ML_Classification/data_csv/ooi_pref_proc/"

    file_list = glob.glob1(DIR, "*.csv")

    for file in file_list:
        # get file name
        name = file.split("_")[1]
        # Remove the prefix in the file name

        os.rename(DIR + file, DIR + name)


def select_and_move_unprefetched_files():
    """
    Auto select the failed prefetched files and move the UN_PREF_DIR
    :return:
    """
    import glob, os
    import shutil

    PREF_DIR = "/PycharmProjects/ML_Classification/data_csv/ooi_pref_proc/"
    ORG_DIR = "/PycharmProjects/ML_Classification/data_csv/ooi_pref/"
    UN_PREF_DIR = "/PycharmProjects/ML_Classification/data_csv/ooi_unp/"

    org_list = glob.glob1(ORG_DIR, "*.csv")
    pref_list = glob.glob1(PREF_DIR, "*.csv")

    # Failed to be prefetched
    unpref_list = list(set(org_list) - set(pref_list))

    # Check if the folder exist
    if not os.path.exists(UN_PREF_DIR):
        os.mkdir(UN_PREF_DIR)

    # Move the unprefetchable file from ORG_DIR to UN_PREF_DIR
    for file in unpref_list:
        shutil.copy(ORG_DIR + file, UN_PREF_DIR + file)




def count_duplicated_request_from_streaming_request():
    """
    There are 16 streaming data objects occupy 72% total requests.
    This function count the total requests at the raw data set "data_proc.csv"
    Then, minuse it with the filterred the streaming request number, is the duplicated request numbers

    :return:
    """
    import pandas as pd

    INPUT_FILE = "/data_csv/data_proc.csv"

    # obj_cand = collections.defaultdict()
    obj_cand = {"0": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0, "8": 0, "9": 0, "10": 0, "11": 0,
                "12": 0, "13": 0, "14": 0, "15": 0}

    counter_line = 0

    df = pd.read_csv(INPUT_FILE, usecols=["userID", "sensor_data"], delimiter=',')

    for index, line in df.iterrows():
        userID = str(line["userID"])
        data_obj = str(line["sensor_data"])

        if counter_line % 100000 == 0:
            print('Line: ', counter_line)
        counter_line += 1

        if userID == "0" and data_obj in obj_cand:
            obj_cand[data_obj] += 1

    cnt_sum = 0

    for obj, val in obj_cand.items():
        print("Obj: %s, Num: %s" % (obj, val))
        cnt_sum += val

    print("Total number: ", cnt_sum)


def find_repeated_data_obj():
    """
    This function check out how many data objects are able to be found from the cache.
    Same data object name, same start and end timestamp

    :return:
    """
    import pandas as pd
    import collections
    import json

    C_SIZE = 1000000
    INPUT_FILE = "/data_csv/data_proc_ts_sorted.csv"

    chunk_counter = 0
    counter_line = 0

    repeat_data_counter = 0

    data_dict = collections.defaultdict()

    for log_df in pd.read_csv(INPUT_FILE, delimiter=',',
                              chunksize=C_SIZE):  # FLAG indicate whether the TS_DIFF is valid
        print(
            "Start reading file chunk #%s" % chunk_counter)  # names=['access_time', 'userID', 'sensor_data', 'begin_time', 'end_time'],
        chunk_counter += 1

        # while line:
        for index, line in log_df.iterrows():
            # print("DEBUG: index %s, line %s " % (index, line) )
            # Report status
            counter_line += 1
            if counter_line % 100000 == 0:
                print('Line: ', counter_line)

            data_name = str(line['sensor_data']) + '#' + str(line['begin_time']) + '#' + str(line['end_time'])

            if data_name in data_dict:
                repeat_data_counter += 1
                data_dict[data_name] += 1
            else:
                data_dict[data_name] = 1

    with open('/data_txt/tmp_data_name_dict.txt', 'w+') as output:
        output.write(json.dumps(data_dict))

    with open('/data_txt/tmp_data_name_output.txt', 'w+') as output_2:
        output_2.write(str(repeat_data_counter))

    print("Found repeated data: ", str(repeat_data_counter))


##########
# Main
########
# get_obj_id_site_stream_pair()
# remove_duplicate_rows_from_csv_file()
# generate_prefetchable_user_obj_dict()
getting_user_access_sequence_and_obj_hit_count()
